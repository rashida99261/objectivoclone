//
//  BaseApi.swift
//  LMS
//
//  Created by Reinforce on 09/12/19.
//  Copyright © 2019 Reinforce. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class BaseApi {
    
    static func onResponsePost(url: String ,parms: NSDictionary, completion: @escaping (_ res:Any) -> Void) {
        
        
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration)
        let url = NSURL(string:"\(url)")
        let request = NSMutableURLRequest(url: url! as URL)
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parms, options: JSONSerialization.WritingOptions())
            let task = session.dataTask(with: request as URLRequest) {
                data, response, error in
                guard let data = data, error == nil else
                {
                    return
                }
                if let httpStatus = response as? HTTPURLResponse,
                    httpStatus.statusCode != 200 { // check for httperrors
                    if let _ = try? JSONSerialization.jsonObject(with: data) as? [String:Any]
                    {
                    }
                }
                if let parsedData = try? JSONSerialization.jsonObject(with: data) as? [String:Any]
                {
                    let success = parsedData["success"] as! Bool
                    if (success == true){
                        
                        completion(parsedData as NSDictionary)
                    }
                    else{
                        completion(parsedData as NSDictionary)
                    }
                }
            }
            task.resume()
        }
        catch
        {
            
        }
    }
    
    static func onResponsePostWithToken(url: String, controller: UIViewController ,parms: Any, completion: @escaping (_ res: Any, _ error: String) -> Void) {
        
        
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration)
        let url = NSURL(string:"\(url)")
        
        
        let request = NSMutableURLRequest(url: url! as URL)
        request.httpMethod = "POST"
        
        let access_token = userDef.object(forKey: "access_token") as! String
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(access_token)", forHTTPHeaderField: "Authorization")
        
        
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parms, options: JSONSerialization.WritingOptions())
            let task = session.dataTask(with: request as URLRequest) {
                data, response, error in
                
                if let httpStatus = response as? HTTPURLResponse,
                    httpStatus.statusCode == 401 { // check for httperrors
                    if let parsedData = try? JSONSerialization.jsonObject(with: data!)
                    {
                        Globalfunc.print(object:parsedData)
                    }
                }
                if error == nil && data != nil{
                    do {
                        let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! [String :AnyObject]
                        //let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments)
                        
                        completion(json, "")
                    }
                    catch {
                        let json : Any = (Any).self
                        completion(json, "Status_Not_200")
                    }
                }
                else{
                    let json : Any = (Any).self
                    completion(json, error.debugDescription)
                }
            }
            task.resume()
        }
        catch
        {
        }
    }
    
    static func callApiRequestForGet(url : String, completionHandler: @escaping (_ result: Any, _ error: String) -> Void){
        
        var searchURL = NSURL()
        if let url = NSURL(string: "\(url)")
        {
            searchURL = url
        }
        else {
            let Nurl : NSString = url as NSString
            let urlStr : NSString = Nurl.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
            searchURL = NSURL(string: urlStr as String)!
        }
        
        var request = URLRequest(url: searchURL as URL)
        let access_token = userDef.object(forKey: "access_token") as! String
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(access_token)", forHTTPHeaderField: "Authorization")
        
        
        URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
            
            if let httpStatus = response as? HTTPURLResponse,
                httpStatus.statusCode == 401 { // check for httperrors
                if let parsedData = try? JSONSerialization.jsonObject(with: data!)
                {
                    let dd = parsedData as! NSDictionary
                    print(dd)
                }
            }
            // Check if data was received successfully
            if error == nil && data != nil {
                do {
                    // Convert NSData to Dictionary where keys are of type String, and values are of any type
                    let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
                    completionHandler(json, "")
                    
                    
                    //do your stuff
                    
                } catch {
                    let json : Any = (Any).self
                    completionHandler(json, "")
                }
            }
            else if error != nil
            {
                let json : Any = (Any).self
                completionHandler(json, "error")
            }
        }).resume()
        
        
        
    }
    
    
    
    static func onResponseMultipartPostWithToken(url: String, controller: UIViewController ,parameters: [String:String],fileName: String, imgData: Data, completion: @escaping (_ res: Any, _ error: String) -> Void)
    {
        
        let access_token = userDef.object(forKey: "access_token") as! String
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(access_token)",
            "Content-type": "multipart/form-data"
        ]
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            // import image to request
            multipartFormData.append(imgData, withName: fileName, fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
            
            for (key, value) in parameters {
                multipartFormData.append((value).data(using: String.Encoding.utf8)!, withName: key)
            }
            
        }, to: url,method:HTTPMethod.post,
           headers:headers,
           encodingCompletion: { encodingResult in
            DispatchQueue.main.async {
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseString { response in
                        Globalfunc.print(object:response)
                        let result = response.result.value
                        completion(result!, "")
                    }
                case .failure(let error):
                    let json : Any = (Any).self
                    completion(json, error.localizedDescription)
                }
            }
        })
    }
    
    
    static func onResponseMultipartImages(url: String, controller: UIViewController ,parameters: [String:String],frntImg: String, frntImgData: Data,bsckImg: String, backImgData: Data, completion: @escaping (_ res: Any, _ error: String) -> Void)
    {
        
        let access_token = userDef.object(forKey: "access_token") as! String
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(access_token)",
            "Content-type": "multipart/form-data"
        ]
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            // import image to request
            multipartFormData.append(frntImgData, withName: frntImg, fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
            
            multipartFormData.append(backImgData, withName: bsckImg, fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
            
            for (key, value) in parameters {
                multipartFormData.append((value).data(using: String.Encoding.utf8)!, withName: key)
            }
            
        }, to: url,method:HTTPMethod.post,
           headers:headers,
           encodingCompletion: { encodingResult in
            DispatchQueue.main.async {
                
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        Globalfunc.print(object:response)
                        let result = response.result.value
                        completion(result!, "")
                    }
                case .failure(let error):
                    let json : Any = (Any).self
                    completion(json, error.localizedDescription)
                    
                }
            }
        })
    }
}
