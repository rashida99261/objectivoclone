//
//  LocationHelper.swift
//  Objectivo
//
//  Created by Apple on 01/09/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit


protocol LocationHelperDelegate{
    func didUpdateLocation(location: CLLocation)
    func didRejectPermission()
}
class LocationHelper:NSObject, CLLocationManagerDelegate{
    
    static let shared = LocationHelper()
    var locationManager = CLLocationManager()
    var delegate:LocationHelperDelegate?
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways || status == .authorizedAlways  {
            if CLLocationManager.isMonitoringAvailable(for: CLBeaconRegion.self) {
                if CLLocationManager.isRangingAvailable() {
                    setLocation()
                }
            }
        }else{
            delegate?.didRejectPermission()
        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last! as CLLocation
        delegate?.didUpdateLocation(location: location)
    }

    //MARK: SET_LOCATION
    func  setLocation(){
        
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.activityType = .fitness
        locationManager.pausesLocationUpdatesAutomatically = true
        locationManager.requestWhenInUseAuthorization()
        
    }
}
