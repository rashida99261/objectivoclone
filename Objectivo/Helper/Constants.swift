//
//  Constants.swift
//  Objectivo
//
//  Created by Apple on 18/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

class Constant{
    
    static let AppName = "OBJECTIVO"
    
    static let base_Url :String = "https://objectivo.in/public/api/"
    
    static let img_base_Url :String = "https://objectivo.in/public"
    
    static let login_with_otp :String = "\(Constant.base_Url)login-with-otp"
    
    static let resend_otp :String = "\(Constant.base_Url)resend-otp"
    
    static let verify_otp :String = "\(Constant.base_Url)verify-otp"
    
    static let get_profile_updated_status :String = "\(Constant.base_Url)get-profile-updated-status"
    
    static let update_basic_details :String = "\(Constant.base_Url)update-basic-details"
    
    static let get_dropdowns :String = "\(Constant.base_Url)get-dropdowns"
    
    static let update_local_address :String = "\(Constant.base_Url)update-local-address"
    
    static let update_permanent_address :String = "\(Constant.base_Url)update-permanent-address"
    
    static let update_kyc_aadhar :String = "\(Constant.base_Url)update-kyc-aadhar"
    
    static let update_kyc_driving_licence :String = "\(Constant.base_Url)update-kyc-driving-licence"
    
    static let update_kyc_pan :String = "\(Constant.base_Url)update-kyc-pan"
    
    static let update_bank_account :String = "\(Constant.base_Url)update-bank-account"
    
    static let send_forgot_password_otp :String = "\(Constant.base_Url)send-forgot-password-otp"
    
    static let verify_forgot_password_otp :String = "\(Constant.base_Url)verify-forgot-password-otp"
    
    static let reset_password :String = "\(Constant.base_Url)reset-password"
    
    static let logout :String = "\(Constant.base_Url)logout"
    
    static let login :String = "\(Constant.base_Url)login"
    
    static let start_day :String = "\(Constant.base_Url)start-day"
    
    static let end_day :String = "\(Constant.base_Url)end-day"
    
    static let get_user_data :String = "\(Constant.base_Url)get-user-data"
    
    
    static let update_packets :String = "\(Constant.base_Url)update-packets"
    
    //supervisor
    
    static let s_start_day :String = "\(Constant.base_Url)supervisor/start-day"
    
    static let get_agent_list :String = "\(Constant.base_Url)supervisor/get-agent-list"
    
    static let s_end_day :String = "\(Constant.base_Url)supervisor/end-day"
    
    static let edit_delivery_type :String = "\(Constant.base_Url)supervisor/edit-delivery-type"
    
    static let agent_view :String = "\(Constant.base_Url)supervisor/agent-view"
    
    
}

class consArrays{
    static var arrStates : [NSDictionary] = []
    static var arrCity :  [NSDictionary] = []
    static var arrBankName :  [NSDictionary] = []
    static var arrDeliveryTyp :  [NSDictionary] = []
    static var arrsupervisor :  [NSDictionary] = []
    static var arrWarehouse :  [NSDictionary] = []
}
