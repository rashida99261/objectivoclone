//
//  popUpView.swift
//  Objectivo
//
//  Created by Apple on 19/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//





import UIKit


class popUpView: UIView {
    

    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMsg: UILabel!
    @IBOutlet weak var btnOk: UIButton!

    
    let nibName = "popUpView"
    var contentView: UIView?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        guard let view = loadViewFromNib() else { return }
        view.frame = self.bounds
        self.addSubview(view)
        contentView = view
        view.frame = self.bounds
        view.backgroundColor=UIColor.clear
        contentView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
    }
    func loadViewFromNib() -> UIView? {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as? UIView
    }
    
    
}
