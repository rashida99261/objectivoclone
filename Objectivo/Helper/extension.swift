//
//  extension.swift
//  Objectivo
//
//  Created by Apple on 18/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

extension UIColor {
    
    func lighter(by percentage: CGFloat = 30.0) -> UIColor? {
        return self.adjust(by: abs(percentage) )
    }
    
    func darker(by percentage: CGFloat = 30.0) -> UIColor? {
        return self.adjust(by: -1 * abs(percentage) )
    }
    
    func adjust(by percentage: CGFloat = 30.0) -> UIColor? {
        var red: CGFloat = 0, green: CGFloat = 0, blue: CGFloat = 0, alpha: CGFloat = 0
        if self.getRed(&red, green: &green, blue: &blue, alpha: &alpha) {
            return UIColor(red: min(red + percentage/100, 1.0),
                           green: min(green + percentage/100, 1.0),
                           blue: min(blue + percentage/100, 1.0),
                           alpha: alpha)
        } else {
            return nil
        }
    }
}

extension UIViewController
{
    func UIColorFromHex(rgbValue:UInt32, alpha:Double=1.0)->UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }
    
    func presentViewControllerBasedOnIdentifier(_ strIdentifier:String, strStoryName: String, animation:Bool){
        
        let story = UIStoryboard.init(name: strStoryName, bundle: nil)
        let destViewController : UIViewController = story.instantiateViewController(withIdentifier: strIdentifier)
        destViewController.modalPresentationStyle = .fullScreen
        self.present(destViewController, animated: animation, completion: nil)
        
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                Globalfunc.print(object:error.localizedDescription)
            }
        }
        return nil
    }
    
    
    func callGetUserProfileApi(){
        BaseApi.callApiRequestForGet(url: Constant.get_user_data) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    if let response = dict as? NSDictionary {
                        if let dictResp = response["data"] as? NSDictionary
                        {
                            do {
                                if #available(iOS 11.0, *) {
                                    let myData = try NSKeyedArchiver.archivedData(withRootObject: dictResp, requiringSecureCoding: false)
                                    userDef.set(myData, forKey: "userDetail")
                                } else {
                                    let myData = NSKeyedArchiver.archivedData(withRootObject: dictResp)
                                    userDef.set(myData, forKey: "userDetail")
                                }
                            } catch {
                                Globalfunc.print(object: "Couldn't write file")
                            }
                        }
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}


extension UIImage {
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }
    
    /// Returns the data for the specified image in JPEG format.
    /// If the image object’s underlying image data has been purged, calling this function forces that data to be reloaded into memory.
    /// - returns: A data object containing the JPEG data, or nil if there was a problem generating the data. This function may return nil if the image has no data or if the underlying CGImageRef contains data in an unsupported bitmap format.
    func jpeg(_ jpegQuality: JPEGQuality) -> Data? {
        return jpegData(compressionQuality: jpegQuality.rawValue)
    }
}
