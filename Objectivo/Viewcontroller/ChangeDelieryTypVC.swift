//
//  ChangeDelieryTypVC.swift
//  Objectivo
//
//  Created by Apple on 01/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class ChangeDelieryTypVC: UIViewController {
    
    @IBOutlet weak var txtDeliveryTyp : UITextField!
    @IBOutlet weak var viewPopUp : UIView!
    @IBOutlet weak var viewAlert : popUpView!
    
    
    var pickerDeliverTyp : UIPickerView!
    
    var delTyp_id = ""
    var pass_Id : Int?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewPopUp.isHidden = true
    }
    
    
    
}


extension ChangeDelieryTypVC {
    
    @IBAction func clickONBackBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickONSaveBtn(_ sender: UIButton)
    {
        
        if(txtDeliveryTyp.text == "" || txtDeliveryTyp.text?.count == 0 || txtDeliveryTyp.text == nil){
            Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: "Select your Delivery type.")
        }
        else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
            let deadlineTime = DispatchTime.now()
            DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                Globalfunc.showLoaderView()
                let param = [
                    "delivery_type_id": self.delTyp_id,
                    "id": self.pass_Id!] as [String:Any]
                Globalfunc.print(object:param)
                self.callPerFormLoginApionBtnLogin(param: param)
            }
        }
        else{
            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
        }
    }
    
    func callPerFormLoginApionBtnLogin(param: [String:Any])
    {
        BaseApi.onResponsePostWithToken(url: Constant.edit_delivery_type, controller: self, parms: param) { (response, error) in
            DispatchQueue.main.async {
                Globalfunc.print(object:response)
                if let dict = response as? NSDictionary{
                    Globalfunc.hideLoaderView()
                    let success = dict["success"] as! Bool
                    let message = dict["message"] as! String
                    if(success == true){
                        self.viewPopUp.isHidden = false
                        self.viewAlert.lblTitle.text = "Success!"
                        self.viewAlert.lblMsg.text = message
                        self.viewAlert.btnOk.addTarget(self, action: #selector(self.clickOnDismissBtn(_:)), for: .touchUpInside)
                    }
                    else{
                        if let _ = dict["message"] as? NSDictionary
                        {
                            Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: "The field is required.")
                            
                        }
                    }
                }
            }
        }
    }
    
    @objc func clickOnDismissBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true) {
            self.viewPopUp.isHidden = true
        }
    }
    
}

extension ChangeDelieryTypVC : UITextFieldDelegate , UIPickerViewDelegate, UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if(pickerView == pickerDeliverTyp){
            return consArrays.arrDeliveryTyp.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if(pickerView == pickerDeliverTyp){
            let dict = consArrays.arrDeliveryTyp[row]
            let strTitle = dict["title"] as! String
            return strTitle
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if(pickerView == pickerDeliverTyp){
            let dict = consArrays.arrDeliveryTyp[row]
            let strTitle = dict["title"] as! String
            self.txtDeliveryTyp.text = strTitle
            let val = dict["value"] as! Int
            delTyp_id = "\(val)"
        }
    }
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField == self.txtDeliveryTyp){
            self.pickUp(txtDeliveryTyp)
        }
    }
    
    func pickUp(_ textField : UITextField) {
        
        if(textField == self.txtDeliveryTyp){
            self.pickerDeliverTyp = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerDeliverTyp.delegate = self
            self.pickerDeliverTyp.dataSource = self
            self.pickerDeliverTyp.backgroundColor = UIColor.white
            textField.inputView = self.pickerDeliverTyp
        }
        // UIPickerView
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(doneClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    //MARK:- Button
    @objc func doneClick() {
        txtDeliveryTyp.resignFirstResponder()
    }
}
