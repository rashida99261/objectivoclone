//
//  profileDetailViewController.swift
//  Objectivo
//
//  Created by Apple on 20/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import SDWebImage

class profileDetailViewController: UIViewController {
    
    @IBOutlet weak var txtFrstName : UITextField!
    @IBOutlet weak var txtLname : UITextField!
    @IBOutlet weak var txtFatherName : UITextField!
    @IBOutlet weak var txtDob : UITextField!
    @IBOutlet weak var txtBloodGroup : UITextField!
    
    @IBOutlet weak var viewImg : UIView!
    @IBOutlet weak var imgProfile : UIImageView!
    @IBOutlet weak var hghtView : NSLayoutConstraint!
    
    var pickerDob : UIDatePicker!
    
    var arrBloodGrup = ["A+","A-","B+","B-","O+","O-","AB+","AB-"]
    var pickerBloodGrup: UIPickerView!
    
    var imagePicker: ImagePicker!
    var imgSelect = ""
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.imagePicker = ImagePicker(presentationController: self, delegate: self)
        self.viewImg.isHidden = true
        self.hghtView.constant = 0
        
        self.setUpUI()
        
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clikONBackBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickOnAddProfileImgBtn(_ sender: UIButton)
    {
        if(sender.tag == 10){
            imgSelect = "true"
            self.imagePicker.present(from: sender)
        }
        if(sender.tag == 20){
            imgSelect = ""
            self.viewImg.isHidden = true
            self.hghtView.constant = 0
            self.imgProfile.image = nil
        }
    }
}

extension profileDetailViewController {
    
    func setUpUI(){
        do {
            if let decoded  = userDef.object(forKey: "userDetail") as? Data{
                if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                    
                    if let first_name = dataD["first_name"] as? String{
                        self.txtFrstName.text = first_name
                    }
                    
                    if let last_name = dataD["last_name"] as? String{
                        self.txtLname.text = last_name
                    }
                    
                    if let fathers_name = dataD["fathers_name"] as? String{
                        self.txtFatherName.text = fathers_name
                    }
                    
                    if let dob = dataD["dob"] as? String{
                        self.txtDob.text = dob
                    }
                    
                    if let blood_group = dataD["blood_group"] as? String{
                        self.txtBloodGroup.text = blood_group
                    }
                    
                    if let profile_image = dataD["profile_image"] as? String{
                        
                        self.viewImg.isHidden = false
                        self.hghtView.constant = 130
                        imgSelect = "true"
                        
                        self.imgProfile.setIndicatorStyle(UIActivityIndicatorView.Style.medium)
                        self.imgProfile.setShowActivityIndicator(true)
                        self.imgProfile.sd_setImage(with: URL.init(string: "\(Constant.img_base_Url)\(profile_image)"), placeholderImage: UIImage(named: ""))
                        
                    }
                }
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
        
    }
}

extension profileDetailViewController {
    
    @IBAction func clickONSaveBtn(_ sender: UIButton)
    {
        if(txtFrstName.text == "" || txtFrstName.text?.count == 0 || txtFrstName.text == nil){
            Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: "First Name should not be left blank.")
        }
        else if(txtLname.text == "" || txtLname.text?.count == 0 || txtLname.text == nil){
            Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: "Last Name should not be left blank.")
        }
        if(txtFatherName.text == "" || txtFatherName.text?.count == 0 || txtFatherName.text == nil){
            Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: "Father Name should not be left blank.")
        }
        else if(txtDob.text == "" || txtDob.text?.count == 0 || txtDob.text == nil){
            Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: "Select your Date of Birth.")
        }
        else if(txtBloodGroup.text == "" || txtBloodGroup.text?.count == 0 || txtBloodGroup.text == nil){
            Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: "Select your Blood Group.")
        }
        else if(imgSelect == ""){
            Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: "Please upload profile Image.")
        }
        else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
            let deadlineTime = DispatchTime.now()
            DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                Globalfunc.showLoaderView()
                
                
                let param = ["first_name": self.txtFrstName.text!,
                             "last_name": self.txtLname.text!,
                             "fathers_name": self.txtFatherName.text!,
                             "dob": self.txtDob.text!,
                             "blood_group": self.txtBloodGroup.text!] as [String:String]
                self.callPerFormLoginApionBtnLogin(param: param)
            }
        }
        else{
            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
        }
    }
    
    func callPerFormLoginApionBtnLogin(param: [String:String])
    {
        if let imgData = self.imgProfile.image?.jpeg(.low) {
            
            BaseApi.onResponseMultipartPostWithToken(url: Constant.update_basic_details, controller: self, parameters: param, fileName: "profile_image", imgData: imgData) { (response, error) in
                
                DispatchQueue.main.async {
                    Globalfunc.hideLoaderView()
                    
                    let dict = self.convertToDictionary(text: response as! String)
                    Globalfunc.print(object:dict!)
                    
                    let success = dict?["success"] as! Bool
                    if(success == true){
                        if let _ = dict?["message"] as? String
                        {
                            self.callGetUserProfileApi()
                            self.presentViewControllerBasedOnIdentifier("localAddressViewController", strStoryName: "Main", animation: true)
                        }
                    }
                    else{
                        if let message = dict?["message"] as? NSDictionary
                        {
                            if let profile_image = message["profile_image"] as? NSArray{
                                let msg = profile_image[0] as! String
                                Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: msg)
                            }
                        }
                    }
                }
            }
        }

        

    }
}


/*MARk - Textfeld and picker delegates*/

extension profileDetailViewController : UITextFieldDelegate , UIPickerViewDelegate, UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if(pickerView == pickerBloodGrup){
            return self.arrBloodGrup.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if(pickerView == pickerBloodGrup){
            let strTitle = self.arrBloodGrup[row]
            return strTitle
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if(pickerView == pickerBloodGrup){
            let strTitle = self.arrBloodGrup[row]
            self.txtBloodGroup.text = strTitle
        }
    }
    //MARK:- TextFiled Delegate
    
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField == self.txtBloodGroup){
            self.pickUp(txtBloodGroup)
        }
        else if(textField == self.txtDob){
            self.pickUpDateTime(txtDob)
        }
    }
    
    func pickUp(_ textField : UITextField) {
        
        if(textField == self.txtBloodGroup){
            self.pickerBloodGrup = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerBloodGrup.delegate = self
            self.pickerBloodGrup.dataSource = self
            self.pickerBloodGrup.backgroundColor = UIColor.white
            textField.inputView = self.pickerBloodGrup
        }
        
        // UIPickerView
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    
    //MARK:- Button
    @objc func doneClick() {
        txtBloodGroup.resignFirstResponder()
    }
    
    @objc func cancelClick() {
        txtBloodGroup.resignFirstResponder()
    }
    
    func pickUpDateTime(_ textField : UITextField) {
        
        if(textField == self.txtDob){
            self.pickerDob = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerDob.datePickerMode = .date
            self.pickerDob.backgroundColor = UIColor.white
            textField.inputView = self.pickerDob
        }
        
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClickDate))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClickDate))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    //MARK:- Button
    @objc func doneClickDate() {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        self.txtDob.text = formatter.string(from: pickerDob.date)
        self.txtDob.resignFirstResponder()
        
    }
    
    @objc func cancelClickDate() {
        txtDob.resignFirstResponder()
    }
}


extension profileDetailViewController : ImagePickerDelegate {
    
    func didSelect(image: UIImage?, imgUrl: URL) {
        self.viewImg.isHidden = false
        self.hghtView.constant = 130
        self.imgProfile.image = image
    }
}
