//
//  bankDetailsViewController.swift
//  Objectivo
//
//  Created by Apple on 22/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class bankDetailsViewController: UIViewController {
    
    
    @IBOutlet weak var txtActNo : UITextField!
    @IBOutlet weak var txtActName : UITextField!
    @IBOutlet weak var txtBankName : UITextField!
    @IBOutlet weak var txtIfscCode : UITextField!
    @IBOutlet weak var txtBranchName : UITextField!
    
    @IBOutlet weak var viewImg : UIView!
    @IBOutlet weak var imgProfile : UIImageView!
    @IBOutlet weak var hghtView : NSLayoutConstraint!
    
    
    
    
    var pickerBankName: UIPickerView!
    
    var imagePicker: ImagePicker!
    var imgSelect = ""
    
    var bank_id = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.imagePicker = ImagePicker(presentationController: self, delegate: self)
        self.viewImg.isHidden = true
        self.hghtView.constant = 0
        // Do any additional setup after loading the view.
        
        self.setUpUI()
    }
    
    @IBAction func clikONBackBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickOnAddProfileImgBtn(_ sender: UIButton)
    {
        if(sender.tag == 10){
            imgSelect = "true"
            self.imagePicker.present(from: sender)
        }
        if(sender.tag == 20){
            imgSelect = ""
            self.viewImg.isHidden = true
            self.hghtView.constant = 0
            self.imgProfile.image = nil
        }
    }
}

extension bankDetailsViewController {
    
    @IBAction func clickONSaveBtn(_ sender: UIButton)
    {
        if(txtActNo.text == "" || txtActNo.text?.count == 0 || txtActNo.text == nil){
            Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: "Account Number should not be left blank.")
        }
        else if(txtBankName.text == "" || txtBankName.text?.count == 0 || txtBankName.text == nil){
            Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: "Bank Name should not be left blank.")
        }
        if(txtActName.text == "" || txtActName.text?.count == 0 || txtActName.text == nil){
            Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: "Bank Account Holder Name should not be left blank.")
        }
        else if(txtIfscCode.text == "" || txtIfscCode.text?.count == 0 || txtIfscCode.text == nil){
            Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: "Ifsc Code should not be left blank.")
        }
        else if(txtBranchName.text == "" || txtBranchName.text?.count == 0 || txtBranchName.text == nil){
            Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: "Branch Name should not be left blank.")
        }
        else if(imgSelect == ""){
            Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: "Please upload cancek cheque Image.")
        }
        else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
            let deadlineTime = DispatchTime.now()
            DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                Globalfunc.showLoaderView()
                let param = ["bank_ac_number": self.txtActNo.text!,
                             "account_holder_name": self.txtActName.text!,
                             "bank_name": "\(self.bank_id)",
                    "ifsc_code": self.txtIfscCode.text!,
                    "branch_name": self.txtBranchName.text!] as [String:String]
                self.callPerFormLoginApionBtnLogin(param: param)
            }
        }
        else{
            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
        }
    }
    
    func callPerFormLoginApionBtnLogin(param: [String:String])
    {
        
        let imgData = self.imgProfile.image?.jpeg(.low)
        
        BaseApi.onResponseMultipartPostWithToken(url: Constant.update_bank_account, controller: self, parameters: param, fileName: "cancel_cheque_snap", imgData: imgData!) { (response, error) in
            
            DispatchQueue.main.async {
                Globalfunc.hideLoaderView()
                
                let dict = self.convertToDictionary(text: response as! String)
                Globalfunc.print(object:dict!)
                
                let success = dict?["success"] as! Bool
                if(success == true){
                    if let _ = dict?["message"] as? String
                    {
                        self.callGetUserProfileApi()
                        self.presentViewControllerBasedOnIdentifier("welcomeViewController", strStoryName: "Main", animation: true)
                    }
                }
                else{
                    if let message = dict?["message"] as? NSDictionary
                    {
                        if let profile_image = message["profile_image"] as? NSArray{
                            let msg = profile_image[0] as! String
                            Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: msg)
                        }
                    }
                }
            }
        }
    }
    
}


/*MARk - Textfeld and picker delegates*/

extension bankDetailsViewController : UITextFieldDelegate , UIPickerViewDelegate, UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if(pickerView == pickerBankName){
            return consArrays.arrBankName.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if(pickerView == pickerBankName){
            let dict = consArrays.arrBankName[row]
            let strTitle = dict["title"] as! String
            return strTitle
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if(pickerView == pickerBankName){
            let dict = consArrays.arrBankName[row]
            let strTitle = dict["title"] as! String
            self.txtBankName.text = strTitle
            bank_id = dict["value"] as! Int
        }
    }
    //MARK:- TextFiled Delegate
    
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField == self.txtBankName){
            self.pickUp(txtBankName)
        }
    }
    
    func pickUp(_ textField : UITextField) {
        
        if(textField == self.txtBankName){
            self.pickerBankName = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerBankName.delegate = self
            self.pickerBankName.dataSource = self
            self.pickerBankName.backgroundColor = UIColor.white
            textField.inputView = self.pickerBankName
        }
        
        // UIPickerView
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    //MARK:- Button
    @objc func doneClick() {
        txtBankName.resignFirstResponder()
    }
    
    @objc func cancelClick() {
        txtBankName.resignFirstResponder()
    }
}


extension bankDetailsViewController : ImagePickerDelegate {
    
    func didSelect(image: UIImage?, imgUrl: URL) {
        self.viewImg.isHidden = false
        self.hghtView.constant = 130
        self.imgProfile.image = image
    }
}

extension bankDetailsViewController {
    
    func setUpUI(){
        do {
            if let decoded  = userDef.object(forKey: "userDetail") as? Data{
                if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                    
                    if let account_holder_name = dataD["account_holder_name"] as? String{
                        self.txtActName.text = account_holder_name
                    }
                    
                    if let bank_ac_number = dataD["bank_ac_number"] as? String{
                        self.txtActNo.text = bank_ac_number
                    }
                    
                    if let bank_name = dataD["bank_name"] as? Int{
                        for i in 0..<consArrays.arrBankName.count{
                            let dict = consArrays.arrBankName[i]
                            let bankId = dict["value"] as! Int
                            if(bank_name == bankId){
                                let strTitle = dict["title"] as! String
                                self.txtBankName.text = strTitle
                                bank_id = bank_name
                                break
                            }
                        }
                    }
                    
                    if let branch_name = dataD["branch_name"] as? String{
                        self.txtBranchName.text = branch_name
                    }
                    
                    if let ifsc_code = dataD["ifsc_code"] as? String{
                        self.txtIfscCode.text = ifsc_code
                    }
                    
                    if let cancel_cheque_snap = dataD["cancel_cheque_snap"] as? String{
                        self.viewImg.isHidden = false
                        self.hghtView.constant = 130
                        imgSelect = "true"
                        self.imgProfile.setIndicatorStyle(UIActivityIndicatorView.Style.medium)
                        self.imgProfile.setShowActivityIndicator(true)
                        self.imgProfile.sd_setImage(with: URL.init(string: "\(Constant.img_base_Url)\(cancel_cheque_snap)"), placeholderImage: UIImage(named: ""))
                    }
                }
            }
            
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
        
    }
}
