//
//  forgetPassViewController.swift
//  Objectivo
//
//  Created by Apple on 22/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class forgetPassViewController: UIViewController {

    
    @IBOutlet weak var txtPhoneNo : UITextField!
    @IBOutlet weak var txtOtp : UITextField!
    
    @IBOutlet weak var btnSend : UIButton!
    
    @IBOutlet weak var viewPopUp : UIView!
    @IBOutlet weak var viewAlert : popUpView!
    
    
    @IBOutlet weak var viewSendOtp : UIView!
    @IBOutlet weak var viewVerifyOtp : UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewPopUp.isHidden = true
        self.viewVerifyOtp.isHidden = true
        self.viewSendOtp.isHidden = false
        self.btnSend.setTitle("Send OTP", for: .normal)
        
    }
    
    @IBAction func clickOnBackBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
}

//Call api
extension forgetPassViewController {
    
    @IBAction func clickOnSendBtn(_ sender: UIButton)
    {
        if let buttonTitle = sender.title(for: .normal) {
            if(buttonTitle == "Send OTP"){
                
                if(txtPhoneNo.text == "" || txtPhoneNo.text?.count == 0 || txtPhoneNo.text == nil){
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Phone should not be left blank.")
                }
                else if(!(Globalfunc.isValidPhone(testStr: txtPhoneNo.text!))){
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Phone number should be of 10 digit.")
                }
                else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
                    
                    let param = ["phone_number":"+91\(self.txtPhoneNo.text!)"] as NSDictionary
                    self.callPerFormLoginApionBtnLogin(strUrl: Constant.send_forgot_password_otp, param: param, strTyp: "otp")
                }
                else{
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
                }
            }
            else if(buttonTitle == "Verify OTP"){
                
                if(txtOtp.text == "" || txtOtp.text?.count == 0 || txtOtp.text == nil){
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "OTP should not be left blank.")
                }
                else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
                    let deadlineTime = DispatchTime.now()
                    DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                        Globalfunc.showLoaderView()
                        
                        let param = ["phone_number":"+91\(self.txtPhoneNo.text!)",
                            "verification_code":self.txtOtp.text!] as NSDictionary
                        self.callPerFormLoginApionBtnLogin(strUrl: Constant.verify_forgot_password_otp, param: param, strTyp: "login")

                    }
                }
                else{
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
                }
            }

        }
    }
        
    
    
    func callPerFormLoginApionBtnLogin(strUrl : String, param: NSDictionary, strTyp: String)
    {
        BaseApi.onResponsePost(url: strUrl, parms: param) { (dict) in
            if(strTyp == "otp"){
                OperationQueue.main.addOperation{
                    Globalfunc.hideLoaderView()
                    if let response = dict as? NSDictionary{
                        let success = response["success"] as! Bool
                        let message = response["message"] as! String
                        if(success == true){
                            self.viewPopUp.isHidden = false
                            self.viewAlert.lblTitle.text = "OTP Send"
                            self.viewAlert.lblMsg.text = "OTP has been send to your number"
                            self.viewAlert.btnOk.addTarget(self, action: #selector(self.clickOnDismissBtn(_:)), for: .touchUpInside)
                        }
                        else{
                            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: message)
                        }
                    }
                }
            }
            else if(strTyp == "login"){
                Globalfunc.print(object:dict)
                OperationQueue.main.addOperation{
                    Globalfunc.hideLoaderView()
                    if let response = dict as? NSDictionary{
                        let success = response["success"] as! Bool
                        let message = response["message"] as! String
                        
                        if(success == true){
                            
                            let alertController = UIAlertController(title: Constant.AppName, message: message, preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                UIAlertAction in
                                let story = UIStoryboard.init(name: "Main", bundle: nil)
                                let reset = story.instantiateViewController(withIdentifier: "resetPassViewController") as! resetPassViewController
                                reset.modalPresentationStyle = .fullScreen
                                reset.strPhoneNo = "+91\(self.txtPhoneNo.text!)"
                                self.present(reset, animated: true, completion: nil)
                            }
                            alertController.addAction(okAction)
                            self.present(alertController, animated: true, completion: nil)
                        }
                        else{
                            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: message)
                        }
                    }
                }
            }
        }
    }
    
    @objc func clickOnDismissBtn(_ sender: UIButton)
    {
        self.viewPopUp.isHidden = true
        self.viewVerifyOtp.isHidden = false
        self.viewSendOtp.isHidden = true
        self.btnSend.setTitle("Verify OTP", for: .normal)
    }
}
