//
//  welcomeSupervisorVC.swift
//  Objectivo
//
//  Created by Apple on 25/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import CoreLocation

class welcomeSupervisorVC: UIViewController , CLLocationManagerDelegate{
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    @IBOutlet weak var txtWarehouseName : UITextField!
    
    var pickerWarehouse : UIPickerView!
    
    var warehouse_id = ""
    
    var locationManager = CLLocationManager()
    
    var currentLat = ""
    var currentLong = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setCurrentDateTime()
        self.callGetApi()
        self.getcurrentLocation()
    }
    
    func setCurrentDateTime()
    {
        let dateFormatter : DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "ddMMMM,yyyy.HH:mm:ss a"
        let date = Date()
        let dateString = dateFormatter.string(from: date)
        let array = dateString.components(separatedBy: ".")
        self.lblDate.text = "\(array[0])"
        self.lblTime.text = "\(array[1])"
    }
    
    func getcurrentLocation(){
        LocationHelper.shared.delegate = self
        LocationHelper.shared.setLocation()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    @IBAction func clickOnStartDayBtn(_ sender: UIButton)
    {
        if(txtWarehouseName.text == "" || txtWarehouseName.text?.count == 0 || txtWarehouseName.text == nil){
            Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: "Select your Warehouse name.")
        }
        else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
            let deadlineTime = DispatchTime.now()
            DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                Globalfunc.showLoaderView()
                let param = [
                    "warehouse_id": self.warehouse_id,
                    "login_latitude": self.currentLat,
                    "login_longitude": self.currentLong
                    ] as [String:Any]
                Globalfunc.print(object:param)
                self.callPerFormLoginApionBtnLogin(param: param)
            }
            
        }
        else{
            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
        }
    }
    
}

extension welcomeSupervisorVC:LocationHelperDelegate{
    func didUpdateLocation(location: CLLocation) {
        self.currentLat = "\(location.coordinate.latitude)"
        self.currentLong = "\(location.coordinate.longitude)"
    }
    
    func didRejectPermission() {
        
    }
}

extension welcomeSupervisorVC {
    
    func callGetApi()
    {
        BaseApi.callApiRequestForGet(url: Constant.get_dropdowns) { (dict, error) in
            if(error == ""){
                Globalfunc.print(object:dict)
                OperationQueue.main.addOperation {
                    if let response = dict as? NSDictionary {
                        if let arrData = response["data"] as? NSDictionary
                        {
                            if let states = arrData["states"] as? [NSDictionary]
                            {
                                consArrays.arrStates = states
                            }
                            
                            if let cities = arrData["cities"] as? [NSDictionary]
                            {
                                consArrays.arrCity = cities
                            }
                            
                            if let banks = arrData["banks"] as? [NSDictionary]
                            {
                                consArrays.arrBankName = banks
                            }
                            
                            if let delivery_type = arrData["delivery_type"] as? [NSDictionary]
                            {
                                consArrays.arrDeliveryTyp = delivery_type
                            }
                            
                            if let supervisors = arrData["supervisors"] as? [NSDictionary]
                            {
                                consArrays.arrsupervisor = supervisors
                            }
                            
                            if let warehouses = arrData["warehouses"] as? [NSDictionary]
                            {
                                consArrays.arrWarehouse = warehouses
                            }
                        }
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}
extension welcomeSupervisorVC {
    
    func callPerFormLoginApionBtnLogin(param: [String:Any])
    {
        BaseApi.onResponsePostWithToken(url: Constant.s_start_day, controller: self, parms: param) { (response, error) in
            DispatchQueue.main.async {
                if let dict = response as? NSDictionary{
                    Globalfunc.hideLoaderView()
                    let success = dict["success"] as! Bool
                    if(success == true){
                        self.presentViewControllerBasedOnIdentifier("supervisorAgentListVC", strStoryName: "Main", animation: true)
                    }
                    else{
                        if let _ = dict["message"] as? NSDictionary
                        {
                            Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: "The field is required.")
                            
                        }
                    }
                }
            }
        }
    }
}

extension welcomeSupervisorVC : UITextFieldDelegate , UIPickerViewDelegate, UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if(pickerView == pickerWarehouse){
            return consArrays.arrWarehouse.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if(pickerView == pickerWarehouse){
            let dict = consArrays.arrWarehouse[row]
            let strTitle = dict["title"] as! String
            return strTitle
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if(pickerView == pickerWarehouse){
            let dict = consArrays.arrWarehouse[row]
            let strTitle = dict["title"] as! String
            self.txtWarehouseName.text = strTitle
            let val = dict["value"] as! Int
            warehouse_id = "\(val)"
            
        }
    }
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField == self.txtWarehouseName){
            self.pickUp(txtWarehouseName)
        }
    }
    
    func pickUp(_ textField : UITextField) {
        
        if(textField == self.txtWarehouseName){
            self.pickerWarehouse = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerWarehouse.delegate = self
            self.pickerWarehouse.dataSource = self
            self.pickerWarehouse.backgroundColor = UIColor.white
            textField.inputView = self.pickerWarehouse
        }
        
        
        // UIPickerView
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(doneClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    //MARK:- Button
    @objc func doneClick() {
        txtWarehouseName.resignFirstResponder()
    }
}
