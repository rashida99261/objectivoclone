//
//  kycPANViewController.swift
//  Objectivo
//
//  Created by Apple on 22/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class kycPANViewController: UIViewController {

    
    
    @IBOutlet weak var txtAdharNo : UITextField!
    @IBOutlet weak var txtNameOnAdhar : UITextField!
    
    @IBOutlet weak var viewFront : UIView!
    @IBOutlet weak var imgfrontAdha : UIImageView!
    @IBOutlet weak var hghtViewFrnt : NSLayoutConstraint!
    
    @IBOutlet weak var viewBack : UIView!
    @IBOutlet weak var imgbackAdha : UIImageView!
    @IBOutlet weak var hghtViewBack : NSLayoutConstraint!
    
    @IBOutlet weak var btnFrnt : UIButton!
    @IBOutlet weak var btnBack : UIButton!
    var selectBtn = ""
    
    var imagePicker: ImagePicker!
    var isFrntSelect = ""
    var isBackSelect = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.imagePicker = ImagePicker(presentationController: self, delegate: self)
        
        self.viewFront.isHidden = true
        self.hghtViewFrnt.constant = 0
        
        self.viewBack.isHidden = true
        self.hghtViewBack.constant = 0
        
        self.setUpUI()
        
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clikONBackBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickONCrossBtn(_ sender: UIButton)
    {
        if(sender.tag == 10){
            self.viewFront.isHidden = true
            self.hghtViewFrnt.constant = 0
            self.imgfrontAdha.image = nil
            
        }
        if(sender.tag == 20){
            self.viewBack.isHidden = true
            self.hghtViewBack.constant = 0
            self.imgbackAdha.image = nil
        }
    }
    
    @IBAction func clickOnAddProfileImgBtn(_ sender: UIButton)
    {
        if(sender == btnFrnt){
            selectBtn = "front"
            isFrntSelect = "true"
        }
        else if(sender == btnBack){
            selectBtn = "back"
            isBackSelect = "true"
        }
        self.imagePicker.present(from: sender)
        
    }
}

extension kycPANViewController {
    
    @IBAction func clickONSaveBtn(_ sender: UIButton)
    {
        if(txtAdharNo.text == "" || txtAdharNo.text?.count == 0 || txtAdharNo.text == nil){
            Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: "PAN Number should not be left blank.")
        }
        else if(txtNameOnAdhar.text == "" || txtNameOnAdhar.text?.count == 0 || txtNameOnAdhar.text == nil){
            Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: "Name should not be left blank.")
        }
        else if(isFrntSelect == ""){
            Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: "Please upload Front Image.")
        }
        else if(isBackSelect == ""){
            Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: "Please upload Back Image.")
        }
            
        else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
            let deadlineTime = DispatchTime.now()
            DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                Globalfunc.showLoaderView()
                
                let param = ["pan_number": self.txtAdharNo.text!,
                             "name_as_on_pancard": self.txtNameOnAdhar.text!
                    ] as [String:String]
                self.callPerFormLoginApionBtnLogin(param: param)
            }
        }
        else{
            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
        }
    }
    
    func callPerFormLoginApionBtnLogin(param: [String:String])
    {
        let imgDataFrnt = self.imgfrontAdha.image?.jpeg(.low)
        let imgDataBack = self.imgbackAdha.image?.jpeg(.low)
        
        BaseApi.onResponseMultipartImages(url: Constant.update_kyc_pan, controller: self, parameters: param, frntImg: "pan_front_snap", frntImgData: imgDataFrnt!, bsckImg: "pan_back_snap", backImgData: imgDataBack!) { (response, error) in
            
            DispatchQueue.main.async {
                Globalfunc.hideLoaderView()
                if let dict = response as? NSDictionary{
                    Globalfunc.print(object:dict)
                    
                    let success = dict["success"] as! Bool
                    if(success == true){
                        if let _ = dict["message"] as? String
                        {
                            self.callGetUserProfileApi()
                            self.presentViewControllerBasedOnIdentifier("bankDetailsViewController", strStoryName: "Main", animation: true)
                        }
                    }
                    else{
                        if let message = dict["message"] as? NSDictionary
                        {
                            if let profile_image = message["profile_image"] as? NSArray{
                                let msg = profile_image[0] as! String
                                Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: msg)
                            }
                        }
                    }
                }
            }
        }
    }
}


extension kycPANViewController : ImagePickerDelegate {
    
    func didSelect(image: UIImage?, imgUrl: URL) {
        
        if(selectBtn == "front"){
            self.viewFront.isHidden = false
            self.hghtViewFrnt.constant = 130
            self.imgfrontAdha.image = image
            
        }
        else if(selectBtn == "back"){
            self.viewBack.isHidden = false
            self.hghtViewBack.constant = 130
            self.imgbackAdha.image = image
            
        }
    }
}


extension kycPANViewController {
    
    func setUpUI(){
        do {
            if let decoded  = userDef.object(forKey: "userDetail") as? Data{
                if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                    
                    if let pan_number = dataD["pan_number"] as? String{
                        self.txtAdharNo.text = pan_number
                    }
                    
                    if let name_as_on_pancard = dataD["name_as_on_pancard"] as? String{
                        self.txtNameOnAdhar.text = name_as_on_pancard
                    }
                    
                    
                    
                    
                    if let pan_front_snap = dataD["pan_front_snap"] as? String{
                        
                        self.viewFront.isHidden = false
                        self.hghtViewFrnt.constant = 130
                        
                        isFrntSelect = "true"
                        
                        self.imgfrontAdha.setIndicatorStyle(UIActivityIndicatorView.Style.medium)
                        self.imgfrontAdha.setShowActivityIndicator(true)
                        self.imgfrontAdha.sd_setImage(with: URL.init(string: "\(Constant.img_base_Url)\(pan_front_snap)"), placeholderImage: UIImage(named: ""))
                        
                    }
                    
                    if let pan_back_snap = dataD["pan_back_snap"] as? String{
                        
                        isBackSelect = "true"
                        self.viewBack.isHidden = false
                        self.hghtViewBack.constant = 130
                        
                        self.imgbackAdha.setIndicatorStyle(UIActivityIndicatorView.Style.medium)
                        self.imgbackAdha.setShowActivityIndicator(true)
                        self.imgbackAdha.sd_setImage(with: URL.init(string: "\(Constant.img_base_Url)\(pan_back_snap)"), placeholderImage: UIImage(named: ""))
                    }
                }
            }
            
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
        
    }
}
