//
//  termsConditionViewController.swift
//  Objectivo
//
//  Created by Apple on 20/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import WebKit

class termsConditionViewController: UIViewController {
    
    @IBOutlet weak var txtTerms : WKWebView!
    var isAccept =  false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let pdf = Bundle.main.url(forResource: "SERVICE PARTNER AGREEMENT", withExtension: "docx", subdirectory: nil, localization: nil)
        txtTerms.load(URLRequest(url: pdf!))
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickOnAceptTermsBtn(_ sender: UIButton)
    {
        if sender.isSelected == true {
            sender.isSelected = false
            isAccept =  false
        }else {
            sender.isSelected = true
            isAccept =  true
        }
    }
    
    @IBAction func clickOnNextBtn(_ sender: UIButton)
    {
        if(isAccept == false){
            Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: "Please accept the terms and conditions.")
        }
        else{
            self.presentViewControllerBasedOnIdentifier("profileDetailViewController", strStoryName: "Main", animation: true) 
        }
    }
}
