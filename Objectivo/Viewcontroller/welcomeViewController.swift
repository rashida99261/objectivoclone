//
//  welcomeViewController.swift
//  Objectivo
//
//  Created by Apple on 22/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit


class welcomeViewController: UIViewController {
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    @IBOutlet weak var viewStart: UIView!
    @IBOutlet weak var viewEnd: UIView!
    
    @IBOutlet weak var lblEndDate: UILabel!
    @IBOutlet weak var lblEndTime: UILabel!
    
    @IBOutlet weak var viewEditPkts : UIView!
    
    @IBOutlet weak var txtPkctNo : UITextField!
    @IBOutlet weak var txtCreturn : UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewEditPkts.alpha = 0
        
        if let _ = userDef.value(forKey: "start_day") as? String{
            self.viewStart.isHidden = true
            self.viewEnd.isHidden = false
        }
        else{
            self.viewStart.isHidden = false
            self.viewEnd.isHidden = true
            
        }
        self.setCurrentDateTime()
        self.callApi(strUrl: Constant.get_dropdowns)
    }
    
    func setCurrentDateTime()
    {
        let dateFormatter : DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "ddMMMM,yyyy.HH:mm:ss a"
        let date = Date()
        let dateString = dateFormatter.string(from: date)
        let array = dateString.components(separatedBy: ".")
        self.lblDate.text = "\(array[0])"
        self.lblTime.text = "\(array[1])"
        self.lblEndDate.text = "\(array[0])"
        self.lblEndTime.text = "\(array[1])"
    }
    
    @IBAction func clickOnInBtn(_ sender: UIButton)
    {
        self.presentViewControllerBasedOnIdentifier("startDayViewController", strStoryName: "Main", animation: true)
    }
    
    @IBAction func clickOnEndDayBtn(_ sender: UIButton)
    {
        self.presentViewControllerBasedOnIdentifier("endDayViewController", strStoryName: "Main", animation: true)
    }
    
    @IBAction func clickOnBottomBtn(_ sender: UIButton)
    {
        self.presentViewControllerBasedOnIdentifier("logoutViewController", strStoryName: "Main", animation: false)
    }
    
    
    @IBAction func clickOnEditBtnPckts(_ sender: UIButton)
    {
        if(sender.tag == 10){
            UIView.animate(withDuration: 1, delay: 0.5, options: .curveEaseInOut, animations: {
                self.viewEditPkts.alpha = 1
            }) { _ in
            }
        }
        else if(sender.tag == 20){
            UIView.animate(withDuration: 1, delay: 0.5, options: .curveEaseInOut, animations: {
            }) { _ in
                
                if(self.txtPkctNo.text == "" || self.txtPkctNo.text?.count == 0 || self.txtPkctNo.text == nil){
                    Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: "No of Packets collected should not be left blank.")
                }
                else if(self.txtCreturn.text == "" || self.txtCreturn.text?.count == 0 || self.txtCreturn.text == nil){
                    Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: "No of C-Return picked should not be left blank.")
                }
                else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
                    let deadlineTime = DispatchTime.now()
                    DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                        Globalfunc.showLoaderView()
                        let param = [
                            "collected_package": self.txtPkctNo.text!,
                            "c_returned_package": self.txtCreturn.text!,
                            ] as [String:Any]
                        Globalfunc.print(object:param)
                        self.callPerFormLoginApionBtnLogin(param: param)
                    }
                }
                else{
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
                }
            }
        }
        else if(sender.tag == 30){
            UIView.animate(withDuration: 1, delay: 0.5, options: .curveEaseInOut, animations: {
                self.viewEditPkts.alpha = 0
            }) { _ in
            }
            
        }
        
    }
    
    func callPerFormLoginApionBtnLogin(param: [String:Any])
    {
        BaseApi.onResponsePostWithToken(url: Constant.update_packets, controller: self, parms: param) { (response, error) in
            DispatchQueue.main.async {
                Globalfunc.print(object:response)
                if let dict = response as? NSDictionary{
                    Globalfunc.hideLoaderView()
                    let success = dict["success"] as! Bool
                    if(success == true){
                        userDef.set(self.txtCreturn.text!, forKey: "Creturn")
                        self.viewEditPkts.alpha = 0
                    }
                    else{
                        if let _ = dict["message"] as? NSDictionary
                        {
                            Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: "The field is required.")
                            
                        }
                    }
                }
            }
        }
    }
    
}

extension welcomeViewController {
    
    func callApi(strUrl: String)
    {
            
            BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
                if(error == ""){
                    Globalfunc.print(object:dict)
                    OperationQueue.main.addOperation {
                        if let response = dict as? NSDictionary {
                            if let arrData = response["data"] as? NSDictionary
                            {
                                if let states = arrData["states"] as? [NSDictionary]
                                {
                                    consArrays.arrStates = states
                                }
                                
                                if let cities = arrData["cities"] as? [NSDictionary]
                                {
                                    consArrays.arrCity = cities
                                }
                                
                                if let banks = arrData["banks"] as? [NSDictionary]
                                {
                                    consArrays.arrBankName = banks
                                }
                                
                                if let delivery_type = arrData["delivery_type"] as? [NSDictionary]
                                {
                                    consArrays.arrDeliveryTyp = delivery_type
                                }
                                
                                if let supervisors = arrData["supervisors"] as? [NSDictionary]
                                {
                                    consArrays.arrsupervisor = supervisors
                                }
                                
                                if let warehouses = arrData["warehouses"] as? [NSDictionary]
                                {
                                    consArrays.arrWarehouse = warehouses
                                }
                                
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                                    self.callGetUserProfileApi()
                                })
                            }
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }

    }
}


