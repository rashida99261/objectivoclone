//
//  supervisorLoginVC.swift
//  Objectivo
//
//  Created by Apple on 30/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class supervisorLoginVC: UIViewController {


    @IBOutlet weak var txtPhoneNo : UITextField!
    @IBOutlet weak var txtPassword : UITextField!
    @IBOutlet weak var txtOtp : UITextField!
    
    @IBOutlet weak var viewPassword : UIView!
    @IBOutlet weak var viewOtp : UIView!
    
    @IBOutlet weak var btnForgetPass : UIButton!
    @IBOutlet weak var btnsendOtp : UIButton!
    @IBOutlet weak var btnLogin : UIButton!
    
    @IBOutlet weak var viewPopUp : UIView!
    @IBOutlet weak var viewAlert : popUpView!
    
    var iconClick = true
    var clickLogin = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewPassword.isHidden = false
        self.viewOtp.isHidden = true
        self.btnsendOtp.isHidden = true
        self.btnForgetPass.isHidden = false
        self.viewPopUp.isHidden = true
    }
    
    @IBAction func clickOnbackBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickOnLoginwithOtp(_ sender: UIButton)
    {
        if sender.isSelected == true {
            clickLogin = false
            sender.isSelected = false
            self.viewPassword.isHidden = false
            self.viewOtp.isHidden = true
            self.btnForgetPass.isHidden = false
            self.btnsendOtp.isHidden = true
            self.btnLogin.backgroundColor = self.UIColorFromHex(rgbValue: 0xED1C24, alpha: 1.0)
            self.btnLogin.isUserInteractionEnabled = true
            
        }
        else {
            clickLogin = true
            sender.isSelected = true
            self.viewPassword.isHidden = true
            self.viewOtp.isHidden = false
            self.btnForgetPass.isHidden = true
            self.btnsendOtp.isHidden = false
            self.btnLogin.backgroundColor = self.UIColorFromHex(rgbValue: 0xED1C24, alpha: 1.0).lighter(by: 50)
            self.btnLogin.isUserInteractionEnabled = false
        }
    }
    
    @IBAction func clickOneyeBtn(_ sender: UIButton)
    {
        if(iconClick == true) {
            txtPassword.isSecureTextEntry = false
        } else {
            txtPassword.isSecureTextEntry = true
        }
        iconClick = !iconClick
    }
    
    @IBAction func clickOnForgetPass(_ sender: UIButton)
    {
        isResetPassFrom = "login"
        self.presentViewControllerBasedOnIdentifier("forgetPassViewController", strStoryName: "Main", animation: true)
    }
    
}

//Call api
extension supervisorLoginVC {
    
    @IBAction func clickOnSendBtn(_ sender: UIButton)
    {
        if(clickLogin == true){
            if(sender.tag == 100){ //otp
                if(txtPhoneNo.text == "" || txtPhoneNo.text?.count == 0 || txtPhoneNo.text == nil){
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Phone should not be left blank.")
                }
                else if(!(Globalfunc.isValidPhone(testStr: txtPhoneNo.text!))){
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Phone number should be of 10 digit.")
                }
                else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
                    let deadlineTime = DispatchTime.now()
                    DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                        Globalfunc.showLoaderView()
                        if let buttonTitle = sender.title(for: .normal) {
                            if(buttonTitle == "Send OTP"){
                                let param = ["phone_number":"+91\(self.txtPhoneNo.text!)",
                                    "user_type":"supervisor"] as NSDictionary
                                
                                self.callPerFormLoginApionBtnLogin(strUrl: Constant.login_with_otp, param: param, strTyp: "otp")
                            }
                            else if(buttonTitle == "Resend OTP"){
                                let param = ["phone_number":"+91\(self.txtPhoneNo.text!)"] as NSDictionary
                                self.callPerFormLoginApionBtnLogin(strUrl: Constant.resend_otp, param: param, strTyp: "otp")
                            }
                        }
                    }
                    
                }
                else{
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
                }
            }
            else if(sender.tag == 200){
                if(txtPhoneNo.text == "" || txtPhoneNo.text?.count == 0 || txtPhoneNo.text == nil){
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Phone should not be left blank.")
                }
                else if(txtOtp.text == "" || txtOtp.text?.count == 0 || txtOtp.text == nil){
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "OTP should not be left blank.")
                }
                else if(!(Globalfunc.isValidPhone(testStr: txtPhoneNo.text!))){
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Phone number should be of 10 digit.")
                }
                else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
                    let deadlineTime = DispatchTime.now()
                    DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                        Globalfunc.showLoaderView()
                        let param = ["phone_number":"+91\(self.txtPhoneNo.text!)",
                            "verification_code":self.txtOtp.text!] as NSDictionary
                        self.callPerFormLoginApionBtnLogin(strUrl: Constant.verify_otp, param: param, strTyp: "login")
                    }
                }
                else{
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
                }
            }
        }
        else if(clickLogin == false){
            
            if(txtPhoneNo.text == "" || txtPhoneNo.text?.count == 0 || txtPhoneNo.text == nil){
                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Phone should not be left blank.")
            }
            else if(txtPassword.text == "" || txtPassword.text?.count == 0 || txtPassword.text == nil){
                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Password should not be left blank.")
            }
            else if(!(Globalfunc.isValidPhone(testStr: txtPhoneNo.text!))){
                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Phone number should be of 10 digit.")
            }
            else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
                let deadlineTime = DispatchTime.now()
                DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                    Globalfunc.showLoaderView()
                    let param = ["phone_number":"+91\(self.txtPhoneNo.text!)",
                        "user_type":"supervisor",
                        "password":self.txtPassword.text!] as NSDictionary
                    Globalfunc.print(object:param)
                    self.callPerFormLoginApionBtnLogin(strUrl: Constant.login, param: param, strTyp: "login_pass")
                }
            }
            else{
                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
            }
        }
    }
    
    func callPerFormLoginApionBtnLogin(strUrl : String, param: NSDictionary, strTyp: String)
    {
        BaseApi.onResponsePost(url: strUrl, parms: param) { (dict) in
            if(strTyp == "otp"){
                OperationQueue.main.addOperation{
                    Globalfunc.hideLoaderView()
                    if let response = dict as? NSDictionary{
                        let success = response["success"] as! Bool
                        let message = response["message"] as! String
                        if(success == true){
                            self.viewPopUp.isHidden = false
                            self.viewAlert.lblTitle.text = "OTP Send"
                            self.viewAlert.lblMsg.text = "OTP has been send to your number"
                            self.viewAlert.btnOk.addTarget(self, action: #selector(self.clickOnDismissBtn(_:)), for: .touchUpInside)
                        }
                        else{
                            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: message)
                        }
                    }
                }
            }
            else if(strTyp == "login"){
                Globalfunc.print(object:dict)
                OperationQueue.main.addOperation{
                    Globalfunc.hideLoaderView()
                    if let response = dict as? NSDictionary{
                        let success = response["success"] as! Bool
                        let message = response["message"] as! String
                        if(success == true){
                            let data = response["data"] as! NSDictionary
                            let access_token = data["access_token"] as! String
                            userDef.set(access_token, forKey: "access_token")
                            
                            let user_type = data["user_type"] as! String
                            userDef.set(user_type, forKey: "user_type")
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
                                self.callGetApi(Constant.get_profile_updated_status)
                            }
                        }
                        else{
                            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: message)
                        }
                    }
                }
            }
            else if(strTyp == "login_pass"){
                Globalfunc.print(object:dict)
                OperationQueue.main.addOperation{
                    Globalfunc.hideLoaderView()
                    if let response = dict as? NSDictionary{
                        let success = response["success"] as! Bool
                        if(success == true){
                            let data = response["data"] as! NSDictionary
                            let access_token = data["access_token"] as! String
                            userDef.set(access_token, forKey: "access_token")
                            let user_type = data["user_type"] as! String
                            userDef.set(user_type, forKey: "user_type")
                            self.presentViewControllerBasedOnIdentifier("welcomeSupervisorVC", strStoryName: "Main", animation: true)
                        }
                        else{
                            if let message = response["message"] as? NSDictionary{
                                if let user_type = message["user_type"] as? [String]{
                                    let msg = user_type[0]
                                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: msg)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    @objc func clickOnDismissBtn(_ sender: UIButton)
    {
        self.viewPopUp.isHidden = true
        self.btnsendOtp.isHidden = false
        self.btnsendOtp.setTitle("Resend OTP", for: .normal)
        self.btnLogin.backgroundColor = self.UIColorFromHex(rgbValue: 0xED1C24, alpha: 1.0)
        self.btnLogin.isUserInteractionEnabled = true
    }
}


extension supervisorLoginVC {
    
    func callGetApi(_ strUrl: String)
    {
        BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    if let response = dict as? NSDictionary {
                        if let data = response["data"] as? NSDictionary{
                            if let profile_status = data["profile_status"] as? [NSDictionary]{
                                let obj = profile_status[0]

                                var bankStatus = ""
                                var basicStatus = ""
                                var kycAharStatus = ""
                                var kycDLStatus = ""
                                var panStatus = ""
                                var localAddr = ""
                                var permAddrs = ""
                                
                                if let bank_account_updated = obj["bank_account_updated"] as? String{
                                    bankStatus = bank_account_updated
                                }
                                if let bank_account_updated = obj["bank_account_updated"] as? Int{
                                    bankStatus = "\(bank_account_updated)"
                                }
                                if let basic_details_updated = obj["basic_details_updated"] as? String{
                                    basicStatus = basic_details_updated
                                }
                                if let basic_details_updated = obj["basic_details_updated"] as? Int{
                                    basicStatus = "\(basic_details_updated)"
                                }
                                if let kyc_aadhar_updated = obj["kyc_aadhar_updated"] as? String{
                                    kycAharStatus = kyc_aadhar_updated
                                }
                                if let kyc_aadhar_updated = obj["kyc_aadhar_updated"] as? Int{
                                    kycAharStatus = "\(kyc_aadhar_updated)"
                                }
                                if let kyc_driving_licence_updated = obj["kyc_driving_licence_updated"] as? String{
                                    kycDLStatus = kyc_driving_licence_updated
                                }
                                if let kyc_driving_licence_updated = obj["kyc_driving_licence_updated"] as? Int{
                                    kycDLStatus = "\(kyc_driving_licence_updated)"
                                }
                                
                                if let kyc_pan_updated = obj["kyc_pan_updated"] as? String{
                                    panStatus = kyc_pan_updated
                                }
                                
                                if let kyc_pan_updated = obj["kyc_pan_updated"] as? Int{
                                    panStatus = "\(kyc_pan_updated)"
                                }
                                
                                if let local_address_updated = obj["local_address_updated"] as? String{
                                    localAddr = local_address_updated
                                }
                                
                                if let local_address_updated = obj["local_address_updated"] as? Int{
                                    localAddr = "\(local_address_updated)"
                                }
                                
                                if let permanent_address_updated = obj["permanent_address_updated"] as? String{
                                    permAddrs = permanent_address_updated
                                }
                                
                                if let permanent_address_updated = obj["permanent_address_updated"] as? Int{
                                    permAddrs = "\(permanent_address_updated)"
                                }
                                if(bankStatus == "0" || basicStatus == "0" || kycAharStatus == "0" || kycDLStatus == "0" || panStatus == "0" || localAddr == "0" || permAddrs == "0"){
                                    self.presentViewControllerBasedOnIdentifier("termsConditionViewController", strStoryName: "Main", animation: true)
                                }
                                else if(bankStatus == "1" || basicStatus == "1" || kycAharStatus == "1" || kycDLStatus == "1" || panStatus == "1" || localAddr == "1" || permAddrs == "1"){
                                    self.presentViewControllerBasedOnIdentifier("welcomeViewController", strStoryName: "Main", animation: true)

                                }
                            }
                        }
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}
