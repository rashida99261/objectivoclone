//
//  supervisorAgentListVC.swift
//  Objectivo
//
//  Created by Apple on 01/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class supervisorAgentListVC: UIViewController {
    
    var arrAgentList : [NSDictionary] = []
    @IBOutlet weak var tblAgentList : UITableView!
    @IBOutlet weak var lblStatus : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblStatus.isHidden = true
        self.tblAgentList.isHidden = true
        
        self.tblAgentList.tableFooterView = UIView()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.arrAgentList = []
        Globalfunc.showLoaderView()
        self.callGetAgentListApi()
        
    }
    
}

extension supervisorAgentListVC {
    
    func callGetAgentListApi()
    {
        BaseApi.callApiRequestForGet(url: Constant.get_agent_list) { (dict, error) in
            if(error == ""){
                Globalfunc.print(object:dict)
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView()
                    if let response = dict as? NSDictionary {
                        if let arrData = response["data"] as? [NSDictionary]
                        {
                            if(arrData.count > 0){
                                for i in 0..<arrData.count{
                                    let dict = arrData[i]
                                    self.arrAgentList.append(dict)
                                }
                                
                                self.lblStatus.isHidden = true
                                self.tblAgentList.isHidden = false
                                self.tblAgentList.reloadData()
                                
                            }
                            else{
                                self.arrAgentList = []
                                self.lblStatus.isHidden = false
                                self.tblAgentList.isHidden = true
                            }
                            
                        }
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}

extension supervisorAgentListVC : UITableViewDelegate , UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrAgentList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tblAgentList.dequeueReusableCell(withIdentifier: "agentTblCell") as! agentTblCell
        
        let dict = self.arrAgentList[indexPath.section]
        
        if let agent_name = dict["agent_name"] as? String{
            cell.lblName.text = "\(indexPath.section + 1). \(agent_name)"
        }
        
        if let delivery_type_name = dict["delivery_type_name"] as? String{
            cell.lbltyp.text = delivery_type_name
        }
        
        cell.btnEdit.tag = indexPath.section
        cell.btnEdit.addTarget(self, action: #selector(clickONBtnEdit(_:)), for: .touchUpInside)
        
        cell.btnView.tag = indexPath.section
        cell.btnView.addTarget(self, action: #selector(clickONBtnDel(_:)), for: .touchUpInside)
        
        return cell
    }
    
    @objc func clickONBtnEdit(_ sender: UIButton)
    {
        let dict = self.arrAgentList[sender.tag]
        let _id = dict["id"] as! Int
        let destViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChangeDelieryTypVC") as! ChangeDelieryTypVC
        destViewController.modalPresentationStyle = .fullScreen
        destViewController.pass_Id = _id
        self.present(destViewController, animated: true, completion: nil)
        
    }
    
    @objc func clickONBtnDel(_ sender: UIButton)
    {
        let dict = self.arrAgentList[sender.tag]
        let _id = dict["id"] as! Int
        let destViewController = self.storyboard?.instantiateViewController(withIdentifier: "viewAgentVC") as! viewAgentVC
        destViewController.modalPresentationStyle = .fullScreen
        destViewController.pass_Id = _id
        self.present(destViewController, animated: true, completion: nil)

    }
}


class agentTblCell: UITableViewCell {
    
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lbltyp : UILabel!
    @IBOutlet weak var btnEdit : UIButton!
    @IBOutlet weak var btnView : UIButton!
    
}


extension supervisorAgentListVC {
    
    @IBAction func clickOnEndDayBtn(_ sender: UIButton)
    {
        self.presentViewControllerBasedOnIdentifier("supervisorEndDayVC", strStoryName: "Main", animation: false)
    }
}
