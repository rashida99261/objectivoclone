//
//  localAddressViewController.swift
//  Objectivo
//
//  Created by Apple on 21/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
var dictLocalAddrs : NSDictionary = [:]

class localAddressViewController: UIViewController {
    
    @IBOutlet weak var txtHousNo : UITextField!
    @IBOutlet weak var txtStreet : UITextField!
    @IBOutlet weak var txtState : UITextField!
    @IBOutlet weak var txtCity : UITextField!
    @IBOutlet weak var txtCountry : UITextField!
    @IBOutlet weak var txtPincode : UITextField!
    
    var pickerState: UIPickerView!
    var pickerCity: UIPickerView!
    
    var state_id = 0
    var city_id = 0
    
    var arrSelcetCity : [NSDictionary] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.txtCountry.text = "India"
        
        let dict = consArrays.arrStates[0]
        let strTitle = dict["title"] as! String
        self.txtState.text = strTitle
        state_id = dict["value"] as! Int
        self.getCityFromState(stateId: "\(state_id)")
        // Do any additional setup after loading the view.
        
        self.setUpUI()
    }
    
    @IBAction func clikONBackBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
}

extension localAddressViewController {
    
    func setUpUI(){
        do {
            if let decoded  = userDef.object(forKey: "userDetail") as? Data{
                if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                    

                    if let local_address_line_1 = dataD["local_address_line_1"] as? String{
                        self.txtHousNo.text = local_address_line_1
                    }
                    
                    if let local_address_line_2 = dataD["local_address_line_2"] as? String{
                        self.txtStreet.text = local_address_line_2
                    }
                    
                    if let local_address_country = dataD["local_address_country"] as? String{
                        self.txtCountry.text = local_address_country
                    }

                    if let local_address_pincode = dataD["local_address_pincode"] as? String{
                        self.txtPincode.text = local_address_pincode
                    }

                    if let local_address_state = dataD["local_address_state"] as? Int{
                        for i in 0..<consArrays.arrStates.count{
                            let dict = consArrays.arrStates[i]
                            let stateId = dict["value"] as! Int
                            if(local_address_state == stateId){
                                let strTitle = dict["title"] as! String
                                self.txtState.text = strTitle
                                state_id = local_address_state
                                self.getCityFromState(stateId: "\(local_address_state)")
                                break
                            }
                        }
                    }
                    
                    if let local_address_city = dataD["local_address_city"] as? String{
                        for i in 0..<self.arrSelcetCity.count
                        {
                            let dict = arrSelcetCity[i]
                            let cityid = dict["value"] as! Int
                            if(local_address_city == "\(cityid)"){
                                let strTitle = dict["title"] as! String
                                self.txtCity.text = strTitle
                                city_id = Int(local_address_city)!
                                break
                            }
                        }
                    }
                }
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }

    }
}

extension localAddressViewController : UITextFieldDelegate , UIPickerViewDelegate, UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if(pickerView == pickerState){
            return consArrays.arrStates.count
        }
        else if(pickerView == pickerCity){
            return arrSelcetCity.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if(pickerView == pickerState){
            let dict = consArrays.arrStates[row]
            let strTitle = dict["title"] as! String
            return strTitle
        }
        else if(pickerView == pickerCity){
            let dict = arrSelcetCity[row]
            let strTitle = dict["title"] as! String
            return strTitle
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if(pickerView == pickerState){
            let dict = consArrays.arrStates[row]
            let strTitle = dict["title"] as! String
            self.txtState.text = strTitle
            state_id = dict["value"] as! Int
            self.getCityFromState(stateId: "\(state_id)")
        }
        else if(pickerView == pickerCity){
            let dict = arrSelcetCity[row]
            let strTitle = dict["title"] as! String
            self.txtCity.text = strTitle
            city_id = dict["value"] as! Int
        }
    }
    
    func getCityFromState(stateId : String)
    {
        self.arrSelcetCity = []
        for i in 0..<consArrays.arrCity.count
        {
            let dic = consArrays.arrCity[i]
            let state_id = dic["state_id"] as! Int
            if("\(state_id)" == stateId){
                self.arrSelcetCity.append(dic)
            }
        }
    }
    
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField == self.txtState){
            self.pickUp(txtState)
        }
        else if(textField == self.txtCity){
            self.pickUp(txtCity)
        }
    }
    
    func pickUp(_ textField : UITextField) {
        
        if(textField == self.txtState){
            self.pickerState = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerState.delegate = self
            self.pickerState.dataSource = self
            self.pickerState.backgroundColor = UIColor.white
            textField.inputView = self.pickerState
        }
        else if(textField == self.txtCity){
            self.pickerCity = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerCity.delegate = self
            self.pickerCity.dataSource = self
            self.pickerCity.backgroundColor = UIColor.white
            textField.inputView = self.pickerCity
        }
        // UIPickerView
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    //MARK:- Button
    @objc func doneClick() {
        txtState.resignFirstResponder()
        txtCity.resignFirstResponder()
    }
    @objc func cancelClick() {
        txtCity.resignFirstResponder()
    }
}


extension localAddressViewController {
    
    @IBAction func clickONSaveBtn(_ sender: UIButton)
    {
        if(txtHousNo.text == "" || txtHousNo.text?.count == 0 || txtHousNo.text == nil){
            Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: "House No. should not be left blank.")
        }
        else if(txtStreet.text == "" || txtStreet.text?.count == 0 || txtStreet.text == nil){
            Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: "Street should not be left blank.")
        }
        if(txtState.text == "" || txtState.text?.count == 0 || txtState.text == nil){
            Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: "Select your State.")
        }
        else if(txtCity.text == "" || txtCity.text?.count == 0 || txtCity.text == nil){
            Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: "Select your City.")
        }
        else if(txtCountry.text == "" || txtCountry.text?.count == 0 || txtCountry.text == nil){
            Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: "Country should not be left blank.")
        }
        else if(txtPincode.text == "" || txtPincode.text?.count == 0 || txtPincode.text == nil){
            Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: "Pincode should not be left blank.")
        }
        else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
            let deadlineTime = DispatchTime.now()
            DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                Globalfunc.showLoaderView()
                let param = ["local_address_line_1": self.txtHousNo.text!,
                             "local_address_line_2": self.txtStreet.text!,
                             "local_address_city": self.city_id,
                             "local_address_state": self.state_id,
                             "local_address_country": self.txtCountry.text!,
                             "local_address_pincode": self.txtPincode.text!] as [String:Any]
                dictLocalAddrs = param as NSDictionary
                self.callPerFormLoginApionBtnLogin(param: param)
            }
        }
        else{
            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
        }
    }
    
    func callPerFormLoginApionBtnLogin(param: [String:Any])
    {
        BaseApi.onResponsePostWithToken(url: Constant.update_local_address, controller: self, parms: param) { (response, error) in
            DispatchQueue.main.async {
                Globalfunc.print(object:response)
                if let dict = response as? NSDictionary{
                    Globalfunc.hideLoaderView()
                    let success = dict["success"] as! Bool
                    if(success == true){
                        if let _ = dict["message"] as? String
                        {
                            self.callGetUserProfileApi()
                            self.presentViewControllerBasedOnIdentifier("permAddressViewController", strStoryName: "Main", animation: true)
                        }
                    }
                    else{
                        if let message = dict["message"] as? NSDictionary
                        {
                            if let profile_image = message["profile_image"] as? NSArray{
                                let msg = profile_image[0] as! String
                                Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: msg)
                            }
                        }
                    }
                }
            }
        }
    }
}
