//
//  startDayViewController.swift
//  Objectivo
//
//  Created by Apple on 25/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import CoreLocation

 

class startDayViewController: UIViewController ,CLLocationManagerDelegate{
    
    @IBOutlet weak var txtWarehouseName : UITextField!
    @IBOutlet weak var txtSupervisorName : UITextField!
    @IBOutlet weak var txtDeliveryTyp : UITextField!
    @IBOutlet weak var txtPckCollected : UITextField!
    @IBOutlet weak var txtCreturn : UITextField!
    
    var pickerWarehouse : UIPickerView!
    var pickerSupervisor : UIPickerView!
    var pickerDeliverTyp : UIPickerView!
    
    var warehouse_id = ""
    var supervisor_id = ""
    var delTyp_id = ""
    
    var locationManager = CLLocationManager()
    
    var currentLat = ""
    var currentLong = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.getcurrentLocation()
    }
    
    
    func getcurrentLocation(){
        LocationHelper.shared.delegate = self
        LocationHelper.shared.setLocation()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
    }
    
}

extension startDayViewController:LocationHelperDelegate{
    func didUpdateLocation(location: CLLocation) {
        self.currentLat = "\(location.coordinate.latitude)"
        self.currentLong = "\(location.coordinate.longitude)"
    }
    
    func didRejectPermission() {
        
    }
}



extension startDayViewController {
    
    @IBAction func clickONBackBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickONSaveBtn(_ sender: UIButton)
    {
        
        if(txtWarehouseName.text == "" || txtWarehouseName.text?.count == 0 || txtWarehouseName.text == nil){
            Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: "Select your Warehouse name.")
        }
        else if(txtSupervisorName.text == "" || txtSupervisorName.text?.count == 0 || txtSupervisorName.text == nil){
            Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: "Select your Supervisor name.")
        }
        if(txtDeliveryTyp.text == "" || txtDeliveryTyp.text?.count == 0 || txtDeliveryTyp.text == nil){
            Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: "Select your Delivery type.")
        }
        else if(txtPckCollected.text == "" || txtPckCollected.text?.count == 0 || txtPckCollected.text == nil){
            Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: "No of Packets collected should not be left blank.")
        }
        else if(txtCreturn.text == "" || txtCreturn.text?.count == 0 || txtCreturn.text == nil){
            Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: "No of C-Return picked should not be left blank.")
        }
        else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
            let deadlineTime = DispatchTime.now()
            DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                Globalfunc.showLoaderView()
                
                let param = [
                    "warehouse_id": self.warehouse_id,
                    "supervisor_id": self.supervisor_id,
                    "delivery_type_id": self.delTyp_id,
                    "collected_package": self.txtPckCollected.text!,
                    "c_returned_package": self.txtCreturn.text!,
                    "login_latitude": self.currentLat,
                    "login_longitude": self.currentLong
                    ] as [String:Any]
                Globalfunc.print(object:param)
                self.callPerFormLoginApionBtnLogin(param: param)
            }
        }
        else{
            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
        }
    }
    
    func callPerFormLoginApionBtnLogin(param: [String:Any])
    {
        BaseApi.onResponsePostWithToken(url: Constant.start_day, controller: self, parms: param) { (response, error) in
            DispatchQueue.main.async {
                Globalfunc.print(object:response)
                if let dict = response as? NSDictionary{
                    Globalfunc.hideLoaderView()
                    let success = dict["success"] as! Bool
                    if(success == true){
                        
                        userDef.set(self.txtCreturn.text!, forKey: "Creturn")
                        userDef.set("start", forKey: "start_day")
                        self.presentViewControllerBasedOnIdentifier("welcomeViewController", strStoryName: "Main", animation: true)
                    }
                    else{
                        if let _ = dict["message"] as? NSDictionary
                        {
                            Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: "The field is required.")
                            
                        }
                    }
                }
            }
        }
    }
}

extension startDayViewController : UITextFieldDelegate , UIPickerViewDelegate, UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if(pickerView == pickerWarehouse){
            return consArrays.arrWarehouse.count
        }
        else if(pickerView == pickerSupervisor){
            return consArrays.arrsupervisor.count
        }
        else if(pickerView == pickerDeliverTyp){
            return consArrays.arrDeliveryTyp.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if(pickerView == pickerWarehouse){
            let dict = consArrays.arrWarehouse[row]
            let strTitle = dict["title"] as! String
            return strTitle
        }
        else if(pickerView == pickerSupervisor){
            let dict = consArrays.arrsupervisor[row]
            var str = "<null>"
            if let strTitle = dict["title"] as? String{
                str = strTitle
            }
            return str
        }
        else if(pickerView == pickerDeliverTyp){
            let dict = consArrays.arrDeliveryTyp[row]
            let strTitle = dict["title"] as! String
            return strTitle
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if(pickerView == pickerWarehouse){
            let dict = consArrays.arrWarehouse[row]
            let strTitle = dict["title"] as! String
            self.txtWarehouseName.text = strTitle
            let val = dict["value"] as! Int
            warehouse_id = "\(val)"
            
        }
        else if(pickerView == pickerSupervisor){
            
            let dict = consArrays.arrsupervisor[row]
            var str = "<null>"
            if let strTitle = dict["title"] as? String{
                str = strTitle
            }
            self.txtSupervisorName.text = str
            let val = dict["value"] as! Int
            supervisor_id = "\(val)"
            
        }
        else if(pickerView == pickerDeliverTyp){
            let dict = consArrays.arrDeliveryTyp[row]
            let strTitle = dict["title"] as! String
            self.txtDeliveryTyp.text = strTitle
            let val = dict["value"] as! Int
            delTyp_id = "\(val)"
        }
    }
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField == self.txtWarehouseName){
            self.pickUp(txtWarehouseName)
        }
        else if(textField == self.txtSupervisorName){
            if(consArrays.arrsupervisor.count == 0){
                self.txtSupervisorName.isUserInteractionEnabled = false
            }
            else{
                self.txtSupervisorName.isUserInteractionEnabled = true
                self.pickUp(txtSupervisorName)
            }
            
            
        }
        else if(textField == self.txtDeliveryTyp){
            self.pickUp(txtDeliveryTyp)
        }
    }
    
    func pickUp(_ textField : UITextField) {
        
        if(textField == self.txtWarehouseName){
            self.pickerWarehouse = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerWarehouse.delegate = self
            self.pickerWarehouse.dataSource = self
            self.pickerWarehouse.backgroundColor = UIColor.white
            textField.inputView = self.pickerWarehouse
        }
        else if(textField == self.txtSupervisorName){
            self.pickerSupervisor = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerSupervisor.delegate = self
            self.pickerSupervisor.dataSource = self
            self.pickerSupervisor.backgroundColor = UIColor.white
            textField.inputView = self.pickerSupervisor
        }
        else if(textField == self.txtDeliveryTyp){
            self.pickerDeliverTyp = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerDeliverTyp.delegate = self
            self.pickerDeliverTyp.dataSource = self
            self.pickerDeliverTyp.backgroundColor = UIColor.white
            textField.inputView = self.pickerDeliverTyp
        }
        
        
        // UIPickerView
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(doneClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    //MARK:- Button
    @objc func doneClick() {
        txtWarehouseName.resignFirstResponder()
        txtSupervisorName.resignFirstResponder()
        txtDeliveryTyp.resignFirstResponder()
    }
}
