//
//  viewAgentVC.swift
//  Objectivo
//
//  Created by Apple on 01/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class viewAgentVC: UIViewController {
    
    @IBOutlet weak var lblAgentName : UILabel!
    @IBOutlet weak var lblTyp : UILabel!
    
    @IBOutlet weak var lblCreturnPkg : UILabel!
    @IBOutlet weak var lblColctedPkg : UILabel!
    @IBOutlet weak var lblLogin : UILabel!
    @IBOutlet weak var lblLogout : UILabel!
    @IBOutlet weak var lblDeliverPkg : UILabel!
    @IBOutlet weak var lblTotalPkgReturn : UILabel!
    
    var pass_Id : Int?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        Globalfunc.showLoaderView()
        self.callPerFormLoginApionBtnLogin()
    }
    
    @IBAction func clickOnBackBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
}

extension viewAgentVC {
    
    func callPerFormLoginApionBtnLogin()
    {
        let url = "\(Constant.agent_view)?id=\(self.pass_Id!)"
        BaseApi.callApiRequestForGet(url: url) { (dict, error) in
            if(error == ""){
                Globalfunc.print(object:dict)
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView()
                    if let response = dict as? NSDictionary {
                        if let arrData = response["data"] as? [NSDictionary]
                        {
                            let dict = arrData[0]
                            self.setUpUI(dict: dict)
                        }
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
    
    func setUpUI(dict: NSDictionary){
        
        if let agent_name = dict["agent_name"] as? String{
            self.lblAgentName.text = agent_name
        }
        
        if let delivery_type_name = dict["delivery_type_name"] as? String{
            self.lblTyp.text = "Delivery Type: \(delivery_type_name)"
        }

        if let login_time = dict["login_time"] as? String{
            self.lblLogin.text = login_time
        }

        if let logout_time = dict["logout_time"] as? String{
            self.lblLogout.text = logout_time
        }
        
        if let c_returned_package = dict["c_returned_package"] as? Int{
            self.lblCreturnPkg.text = "\(c_returned_package)"
            self.lblTotalPkgReturn.text = "\(c_returned_package)"
        }

        if let collected_package = dict["collected_package"] as? Int{
            self.lblColctedPkg.text = "\(collected_package)"
        }

        if let delivered_package = dict["delivered_package"] as? Int{
            self.lblDeliverPkg.text = "\(delivered_package)"
        }


    }
}
