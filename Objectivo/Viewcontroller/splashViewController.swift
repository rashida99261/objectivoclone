//
//  splashViewController.swift
//  Objectivo
//
//  Created by Apple on 20/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class splashViewController: UIViewController {
    
    var window: UIWindow?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            self.gotTOLandingPage()
        }
    }
}

extension splashViewController {
    
    func gotTOLandingPage()
    {
        userDef.removeObject(forKey: "start_day")
        if let _ = userDef.object(forKey: "access_token") as? String {
            //call get status Api
            DispatchQueue.main.asyncAfter(deadline: .now()) {
                self.callGetApi(Constant.get_dropdowns, strTyp: "dropdown")
            }
        }
        else {
            self.presentViewControllerBasedOnIdentifier("LoginViewController", strStoryName: "Main", animation: true)
        }
    }
    
    func callGetApi(_ strUrl: String, strTyp: String)
    {
        BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
            if(error == ""){
                if(strTyp == "dropdown"){
                    OperationQueue.main.addOperation {
                        if let response = dict as? NSDictionary {
                            if let arrData = response["data"] as? NSDictionary
                            {
                                if let states = arrData["states"] as? [NSDictionary]
                                {
                                    consArrays.arrStates = states
                                }
                                
                                if let cities = arrData["cities"] as? [NSDictionary]
                                {
                                    consArrays.arrCity = cities
                                }
                                
                                if let banks = arrData["banks"] as? [NSDictionary]
                                {
                                    consArrays.arrBankName = banks
                                }
                                
                                if let delivery_type = arrData["delivery_type"] as? [NSDictionary]
                                {
                                    consArrays.arrDeliveryTyp = delivery_type
                                }
                                
                                if let supervisors = arrData["supervisors"] as? [NSDictionary]
                                {
                                    consArrays.arrsupervisor = supervisors
                                }
                                
                                if let warehouses = arrData["warehouses"] as? [NSDictionary]
                                {
                                    consArrays.arrWarehouse = warehouses
                                }
                                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                                    if let user_type = userDef.object(forKey: "user_type") as? String {
                                        if(user_type == "agent"){
                                            self.callGetApi(Constant.get_profile_updated_status, strTyp: "status")
                                        }
                                        else if(user_type == "supervisor"){
                                            self.setRootViewController(viewIdentifier: "welcomeSupervisorVC")
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                    
                else if(strTyp == "status"){
                    
                    OperationQueue.main.addOperation {
                        if let response = dict as? NSDictionary {
                            if let data = response["data"] as? NSDictionary{
                                
                                Globalfunc.print(object:data)
                                
                                if let profile_status = data["profile_status"] as? [NSDictionary]{
                                    let obj = profile_status[0]
                                    
                                    var bankStatus = ""
                                    var basicStatus = ""
                                    var kycAharStatus = ""
                                    var kycDLStatus = ""
                                    var panStatus = ""
                                    var localAddr = ""
                                    var permAddrs = ""
                                    
                                    if let bank_account_updated = obj["bank_account_updated"] as? String{
                                        bankStatus = bank_account_updated
                                    }
                                    
                                    if let bank_account_updated = obj["bank_account_updated"] as? Int{
                                        bankStatus = "\(bank_account_updated)"
                                    }
                                    
                                    if let basic_details_updated = obj["basic_details_updated"] as? String{
                                        basicStatus = basic_details_updated
                                    }
                                    
                                    if let basic_details_updated = obj["basic_details_updated"] as? Int{
                                        basicStatus = "\(basic_details_updated)"
                                    }
                                    
                                    if let kyc_aadhar_updated = obj["kyc_aadhar_updated"] as? String{
                                        kycAharStatus = kyc_aadhar_updated
                                    }
                                    
                                    if let kyc_aadhar_updated = obj["kyc_aadhar_updated"] as? Int{
                                        kycAharStatus = "\(kyc_aadhar_updated)"
                                    }
                                    
                                    if let kyc_driving_licence_updated = obj["kyc_driving_licence_updated"] as? String{
                                        kycDLStatus = kyc_driving_licence_updated
                                    }
                                    
                                    if let kyc_driving_licence_updated = obj["kyc_driving_licence_updated"] as? Int{
                                        kycDLStatus = "\(kyc_driving_licence_updated)"
                                    }
                                    
                                    if let kyc_pan_updated = obj["kyc_pan_updated"] as? String{
                                        panStatus = kyc_pan_updated
                                    }
                                    
                                    if let kyc_pan_updated = obj["kyc_pan_updated"] as? Int{
                                        panStatus = "\(kyc_pan_updated)"
                                    }
                                    
                                    if let local_address_updated = obj["local_address_updated"] as? String{
                                        localAddr = local_address_updated
                                    }
                                    
                                    if let local_address_updated = obj["local_address_updated"] as? Int{
                                        localAddr = "\(local_address_updated)"
                                    }
                                    
                                    if let permanent_address_updated = obj["permanent_address_updated"] as? String{
                                        permAddrs = permanent_address_updated
                                    }
                                    
                                    if let permanent_address_updated = obj["permanent_address_updated"] as? Int{
                                        permAddrs = "\(permanent_address_updated)"
                                    }
                                    
                                    if(bankStatus == "0" || basicStatus == "0" || kycAharStatus == "0" || kycDLStatus == "0" || panStatus == "0" || localAddr == "0" || permAddrs == "0"){
                                        self.setRootViewController(viewIdentifier: "termsConditionViewController")
                                    }
                                    else if(bankStatus == "1" || basicStatus == "1" || kycAharStatus == "1" || kycDLStatus == "1" || panStatus == "1" || localAddr == "1" || permAddrs == "1"){
                                         self.setRootViewController(viewIdentifier: "welcomeViewController")
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
    func setRootViewController(viewIdentifier: String)
    {
        let _window = UIWindow(frame: UIScreen.main.bounds)
        window = _window
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let initialViewController : UIViewController = storyboard.instantiateViewController(withIdentifier: viewIdentifier)
        let rootViewController = initialViewController
        let navigationController = UINavigationController()
        navigationController.navigationBar.isHidden = true
        navigationController.viewControllers = [rootViewController]
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
    }
}
