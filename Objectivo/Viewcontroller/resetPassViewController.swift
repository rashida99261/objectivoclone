//
//  resetPassViewController.swift
//  Objectivo
//
//  Created by Apple on 25/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class resetPassViewController: UIViewController {
    
    
    var strPhoneNo = ""
    
    @IBOutlet weak var txtPassword : UITextField!
    @IBOutlet weak var txtCnfrmPass : UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func clickONBackBtn(_ sender: UIButton)
    {
        self.presentViewControllerBasedOnIdentifier("logoutViewController", strStoryName: "Main", animation: false)
    }
    
    
}

//Call api
extension resetPassViewController {
    
    @IBAction func clickOnSendBtn(_ sender: UIButton)
    {
        
        if(txtPassword.text == "" || txtPassword.text?.count == 0 || txtPassword.text == nil){
            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Password should not be left blank.")
        }
        else if(txtCnfrmPass.text == "" || txtCnfrmPass.text?.count == 0 || txtCnfrmPass.text == nil){
            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Password should not be left blank.")
        }
            
        else if(txtCnfrmPass.text != txtPassword.text)
        {
            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Passwords do not match.")
        }
            
        else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
            let deadlineTime = DispatchTime.now()
            DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                Globalfunc.showLoaderView()
                let param = ["phone_number": self.strPhoneNo,
                             "password": self.txtPassword.text!,
                             "password_confirmation": self.txtCnfrmPass.text!] as NSDictionary
                self.callPerFormLoginApionBtnLogin(strUrl: Constant.reset_password, param: param)
                
            }
        }
        else{
            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
        }
    }
    
    
    
    func callPerFormLoginApionBtnLogin(strUrl : String, param: NSDictionary)
    {
        BaseApi.onResponsePost(url: strUrl, parms: param) { (dict) in
            Globalfunc.print(object:dict)
            OperationQueue.main.addOperation{
                Globalfunc.hideLoaderView()
                if let response = dict as? NSDictionary{
                    let success = response["success"] as! Bool
                    if(success == true){
                        let _ = response["message"] as! String
                        if(isResetPassFrom == "welcome"){
                            self.presentViewControllerBasedOnIdentifier("logoutViewController", strStoryName: "Main", animation: false)
                        }
                        else if(isResetPassFrom == "login"){
                            self.presentViewControllerBasedOnIdentifier("LoginViewController", strStoryName: "Main", animation: true)
                        }
                    }
                    else{
                        let message = response["message"] as! NSDictionary
                        if let password = message["password"] as? NSArray
                        {
                            let str =  password[0] as! String
                            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr:str)

                        }
                    }
                }
            }
        }
    }
}
