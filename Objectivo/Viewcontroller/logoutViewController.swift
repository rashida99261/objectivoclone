//
//  logoutViewController.swift
//  Objectivo
//
//  Created by Apple on 25/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class logoutViewController: UIViewController {
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setCurrentDateTime()
    }
    
    func setCurrentDateTime()
    {
        let dateFormatter : DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "ddMMMM,yyyy.HH:mm:ss a"
        let date = Date()
        let dateString = dateFormatter.string(from: date)
        let array = dateString.components(separatedBy: ".")
        self.lblDate.text = "\(array[0])"
        self.lblTime.text = "\(array[1])"
    }
    
    @IBAction func clickOnLogoutBtn(_ sender: UIButton)
    {
        if(sender.tag == 10){
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()) {
                Globalfunc.showLoaderView()
                self.callLogoutApi()
            }
        }
        else if(sender.tag == 20){
            isResetPassFrom = "welcome"
            self.presentViewControllerBasedOnIdentifier("forgetPassViewController", strStoryName: "Main", animation: true)
        }
        else if(sender.tag == 30){
            self.presentViewControllerBasedOnIdentifier("profileDetailViewController", strStoryName: "Main", animation: true)
        }
    }
    
    @IBAction func clickOnBottomBtn(_ sender: UIButton)
    {
          self.presentViewControllerBasedOnIdentifier("welcomeViewController", strStoryName: "Main", animation: false)
    }
}

extension logoutViewController
{
    func callLogoutApi()
    {
            
        BaseApi.callApiRequestForGet(url: Constant.logout) { (dict, error) in
                if(error == ""){
                    Globalfunc.print(object:dict)
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView()
                        if let response = dict as? NSDictionary{
                            let success = response["success"] as! Bool
                            let message = response["message"] as! String
                            if(success == true){
                                userDef.removeObject(forKey: "access_token")
                                userDef.removeObject(forKey: "user_type")
                                self.presentViewControllerBasedOnIdentifier("splashViewController", strStoryName: "Main", animation: true)
                            }
                            else{
                                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: message)
                            }
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
    }
}
