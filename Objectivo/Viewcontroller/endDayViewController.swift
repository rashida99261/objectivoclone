//
//  endDayViewController.swift
//  Objectivo
//
//  Created by Apple on 01/09/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import CoreLocation

class endDayViewController: UIViewController , CLLocationManagerDelegate{
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var txtPckDeliver: UITextField!
    @IBOutlet weak var lblReturnPckts: UILabel!
   
    var Creturn_calc = ""
    
    var locationManager = CLLocationManager()
    
    var currentLat = ""
    var currentLong = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if let c_return = userDef.value(forKey: "Creturn") as? String{
            self.lblReturnPckts.text = c_return
        }
        else{
            self.lblReturnPckts.text = "0"
        }
        
        
        self.setCurrentDateTime()
        self.getcurrentLocation()
    }
    
    
    func getcurrentLocation(){
        LocationHelper.shared.delegate = self
        LocationHelper.shared.setLocation()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    func setCurrentDateTime()
    {
        let dateFormatter : DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "ddMMMM,yyyy.HH:mm:ss a"
        let date = Date()
        let dateString = dateFormatter.string(from: date)
        let array = dateString.components(separatedBy: ".")
        self.lblDate.text = "\(array[0])"
        self.lblTime.text = "\(array[1])"
    }
}

extension endDayViewController:LocationHelperDelegate{
    func didUpdateLocation(location: CLLocation) {
        self.currentLat = "\(location.coordinate.latitude)"
        self.currentLong = "\(location.coordinate.longitude)"
    }
    
    func didRejectPermission() {
        
    }
}


extension endDayViewController {
    
    @IBAction func clickONBackBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickONSaveBtn(_ sender: UIButton)
    {
        if(txtPckDeliver.text == "" || txtPckDeliver.text?.count == 0 || txtPckDeliver.text == nil){
            Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: "No of Packets delivered should not be left blank.")
        }
        else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
            let deadlineTime = DispatchTime.now()
            DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                Globalfunc.showLoaderView()
                let Del = Int(self.txtPckDeliver.text!)
                let Cret = Int(self.lblReturnPckts.text!)!
                let calc = Del! - Cret
                self.Creturn_calc = "\(calc)"
                Globalfunc.print(object:self.Creturn_calc)
                let param = [
                    "c_returned_package_collected": self.Creturn_calc,
                    "delivered_package": self.txtPckDeliver.text!,
                    "logout_latitude": self.currentLat,
                    "logout_longitude": self.currentLong
                    ] as [String:Any]
                Globalfunc.print(object:param)
                self.callPerFormLoginApionBtnLogin(param: param)
            }
        }
        else{
            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
        }
    }
    
    func callPerFormLoginApionBtnLogin(param: [String:Any])
    {
        BaseApi.onResponsePostWithToken(url: Constant.end_day, controller: self, parms: param) { (response, error) in
            DispatchQueue.main.async {
                Globalfunc.print(object:response)
                if let dict = response as? NSDictionary{
                    Globalfunc.hideLoaderView()
                    let success = dict["success"] as! Bool
                    if(success == true){
                        userDef.removeObject(forKey: "access_token")
                        userDef.removeObject(forKey: "user_type")
                        self.presentViewControllerBasedOnIdentifier("splashViewController", strStoryName: "Main", animation: true)
                    }
                    else{
                        if let _ = dict["message"] as? NSDictionary
                        {
                            Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: "The field is required.")
                            
                        }
                    }
                }
                else{
                    Globalfunc.hideLoaderView()
                    Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: "Something went wrong.")
                }
            }
        }
    }
}

