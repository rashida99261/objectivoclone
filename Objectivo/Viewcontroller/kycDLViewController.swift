//
//  kycDLViewController.swift
//  Objectivo
//
//  Created by Apple on 22/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class kycDLViewController: UIViewController {
    
    @IBOutlet weak var txtDlNo : UITextField!
    @IBOutlet weak var txtDlname : UITextField!
    @IBOutlet weak var txtDob : UITextField!
    @IBOutlet weak var txtIssueDate : UITextField!
    @IBOutlet weak var txtExpiryDate : UITextField!
    @IBOutlet weak var txtState : UITextField!
    
    @IBOutlet weak var viewFront : UIView!
    @IBOutlet weak var imgfrontdl : UIImageView!
    @IBOutlet weak var hghtViewFrnt : NSLayoutConstraint!
    
    @IBOutlet weak var viewBack : UIView!
    @IBOutlet weak var imgbackdl : UIImageView!
    @IBOutlet weak var hghtViewBack : NSLayoutConstraint!
    
    @IBOutlet weak var btnFrnt : UIButton!
    @IBOutlet weak var btnBack : UIButton!
    var selectBtn = ""
    
    var imagePicker: ImagePicker!
    var isFrntSelect = ""
    var isBackSelect = ""
    
    var pickerDob : UIDatePicker!
    var pickerIssueDate : UIDatePicker!
    var pickerExpDate : UIDatePicker!
    
    var pickerState: UIPickerView!
    
    var state_id = 0
    
    var selectDate = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.imagePicker = ImagePicker(presentationController: self, delegate: self)
        
        self.viewFront.isHidden = true
        self.hghtViewFrnt.constant = 0
        
        self.viewBack.isHidden = true
        self.hghtViewBack.constant = 0
        
        self.setUpUI()
        
        //        let dict = consArrays.arrStates[0]
        //        let strTitle = dict["title"] as! String
        //        self.txtState.text = strTitle
        //        state_id = dict["value"] as! String
        
        
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clikONBackBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickONCrossBtn(_ sender: UIButton)
    {
        if(sender.tag == 10){
            self.viewFront.isHidden = true
            self.hghtViewFrnt.constant = 0
            self.imgfrontdl.image = nil
            
        }
        if(sender.tag == 20){
            self.viewBack.isHidden = true
            self.hghtViewBack.constant = 0
            self.imgbackdl.image = nil
        }
    }
    
    @IBAction func clickOnAddProfileImgBtn(_ sender: UIButton)
    {
        if(sender == btnFrnt){
            selectBtn = "front"
            isFrntSelect = "true"
        }
        else if(sender == btnBack){
            selectBtn = "back"
            isBackSelect = "true"
        }
        self.imagePicker.present(from: sender)
        
    }
}

extension kycDLViewController {
    
    @IBAction func clickONSaveBtn(_ sender: UIButton)
    {
        if(txtDlNo.text == "" || txtDlNo.text?.count == 0 || txtDlNo.text == nil){
            Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: "Driving Licence Number should not be left blank.")
        }
        else if(txtDlname.text == "" || txtDlname.text?.count == 0 || txtDlname.text == nil){
            Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: "Name should not be left blank.")
        }
        else if(txtDob.text == "" || txtDob.text?.count == 0 || txtDob.text == nil){
            Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: "Select your Date of Birth.")
        }
            
        else if(txtIssueDate.text == "" || txtIssueDate.text?.count == 0 || txtIssueDate.text == nil){
            Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: "Select your card Issue Date.")
        }
            
        else if(txtExpiryDate.text == "" || txtExpiryDate.text?.count == 0 || txtExpiryDate.text == nil){
            Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: "Select your card Expiry Date.")
        }
        
        if(txtState.text == "" || txtState.text?.count == 0 || txtState.text == nil){
            Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: "Select your State.")
        }
            
            
        else if(isFrntSelect == ""){
            Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: "Please upload Front Image.")
        }
        else if(isBackSelect == ""){
            Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: "Please upload Back Image.")
        }
            
        else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
            let deadlineTime = DispatchTime.now()
            DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                Globalfunc.showLoaderView()
                let param = ["driving_licence_number": self.txtDlNo.text!,
                             "name_as_on_dl": self.txtDlname.text!,
                             "dob_dl": self.txtDob.text!,
                             "dl_issue_date": self.txtIssueDate.text!,
                             "dl_expiry_date": self.txtExpiryDate.text!,
                             "issued_state": "\(self.state_id)"
                    ] as [String:String]
                self.callPerFormLoginApionBtnLogin(param: param)
            }
        }
        else{
            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
        }
    }
    
    func callPerFormLoginApionBtnLogin(param: [String:String])
    {
        
        let imgDataFrnt = self.imgfrontdl.image?.jpeg(.low)
        let imgDataBack = self.imgbackdl.image?.jpeg(.low)

        BaseApi.onResponseMultipartImages(url: Constant.update_kyc_driving_licence, controller: self, parameters: param, frntImg: "dl_front_snap", frntImgData: imgDataFrnt!, bsckImg: "dl_back_snap", backImgData: imgDataBack!) { (response, error) in
            
            DispatchQueue.main.async {
                Globalfunc.hideLoaderView()
                if let dict = response as? NSDictionary{
                    Globalfunc.print(object:dict)
                    
                    let success = dict["success"] as! Bool
                    if(success == true){
                        if let _ = dict["message"] as? String
                        {
                            self.callGetUserProfileApi()
                            self.presentViewControllerBasedOnIdentifier("kycPANViewController", strStoryName: "Main", animation: true)
                        }
                    }
                    else{
                        if let message = dict["message"] as? NSDictionary
                        {
                            if let profile_image = message["profile_image"] as? NSArray{
                                let msg = profile_image[0] as! String
                                Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: msg)
                            }
                        }
                    }
                }
            }
        }
    }
}

/*MARk - Textfeld and picker delegates*/

extension kycDLViewController : UITextFieldDelegate , UIPickerViewDelegate, UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if(pickerView == pickerState){
            return consArrays.arrStates.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if(pickerView == pickerState){
            let dict = consArrays.arrStates[row]
            let strTitle = dict["title"] as! String
            return strTitle
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if(pickerView == pickerState){
            
            let dict = consArrays.arrStates[row]
            let strTitle = dict["title"] as! String
            self.txtState.text = strTitle
            state_id = dict["value"] as! Int
        }
    }
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField == self.txtState){
            self.pickUp(txtState)
        }
        else if(textField == self.txtDob){
            self.pickUpDateTime(txtDob)
        }
        else if(textField == self.txtIssueDate){
            self.pickUpDateTime(txtIssueDate)
        }
        else if(textField == self.txtExpiryDate){
            self.pickUpDateTime(txtExpiryDate)
        }
        
    }
    
    func pickUp(_ textField : UITextField) {
        
        if(textField == self.txtState){
            self.pickerState = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerState.delegate = self
            self.pickerState.dataSource = self
            self.pickerState.backgroundColor = UIColor.white
            textField.inputView = self.pickerState
        }
        
        // UIPickerView
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    
    //MARK:- Button
    @objc func doneClick() {
        txtState.resignFirstResponder()
    }
    
    @objc func cancelClick() {
        txtState.resignFirstResponder()
    }
    
    func pickUpDateTime(_ textField : UITextField) {
        
        if(textField == self.txtDob){
            self.pickerDob = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerDob.datePickerMode = .date
            self.pickerDob.backgroundColor = UIColor.white
            textField.inputView = self.pickerDob
            self.selectDate = "dob"
        }
        else if(textField == self.txtIssueDate){
            self.pickerIssueDate = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerIssueDate.datePickerMode = .date
            self.pickerIssueDate.backgroundColor = UIColor.white
            textField.inputView = self.pickerIssueDate
            self.selectDate = "issue"
        }
        else if(textField == self.txtExpiryDate){
            self.pickerExpDate = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerExpDate.datePickerMode = .date
            self.pickerExpDate.backgroundColor = UIColor.white
            textField.inputView = self.pickerExpDate
            self.selectDate = "expire"
        }
        
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClickDate))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClickDate))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    //MARK:- Button
    @objc func doneClickDate() {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        
        if(selectDate == "dob"){
            self.txtDob.text = formatter.string(from: pickerDob.date)
            self.txtDob.resignFirstResponder()
            
        }
        else if(selectDate == "issue"){
            self.txtIssueDate.text = formatter.string(from: pickerIssueDate.date)
            self.txtIssueDate.resignFirstResponder()
        }
        else if(selectDate == "expire"){
            self.txtExpiryDate.text = formatter.string(from: pickerExpDate.date)
            self.txtExpiryDate.resignFirstResponder()
        }
    }
    
    @objc func cancelClickDate() {
        txtDob.resignFirstResponder()
        self.txtIssueDate.resignFirstResponder()
        self.txtExpiryDate.resignFirstResponder()
    }
}


extension kycDLViewController : ImagePickerDelegate {
    
    func didSelect(image: UIImage?, imgUrl: URL) {
        
        if(selectBtn == "front"){
            self.viewFront.isHidden = false
            self.hghtViewFrnt.constant = 130
            self.imgfrontdl.image = image
            
        }
        else if(selectBtn == "back"){
            self.viewBack.isHidden = false
            self.hghtViewBack.constant = 130
            self.imgbackdl.image = image
            
        }
    }
}

extension kycDLViewController {
    
    func setUpUI(){
        do {
            if let decoded  = userDef.object(forKey: "userDetail") as? Data{
                if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                    
                    if let driving_licence_number = dataD["driving_licence_number"] as? String{
                        self.txtDlNo.text = driving_licence_number
                    }
                    
                    if let name_as_on_dl = dataD["name_as_on_dl"] as? String{
                        self.txtDlname.text = name_as_on_dl
                    }
                    
                    if let dob_dl = dataD["dob_dl"] as? String{
                        self.txtDob.text = dob_dl
                    }
                    
                    if let dl_expiry_date = dataD["dl_expiry_date"] as? String{
                        self.txtExpiryDate.text = dl_expiry_date
                    }
                    
                    if let dl_issue_date = dataD["dl_issue_date"] as? String{
                        self.txtIssueDate.text = dl_issue_date
                    }
                    
                    if let issued_state = dataD["issued_state"] as? Int{
                        for i in 0..<consArrays.arrStates.count{
                            let dict = consArrays.arrStates[i]
                            let stateId = dict["value"] as! Int
                            if(issued_state == stateId){
                                let strTitle = dict["title"] as! String
                                self.txtState.text = strTitle
                                state_id = issued_state
                                break
                            }
                        }
                    }
                    
                    
                    if let dl_front_snap = dataD["dl_front_snap"] as? String{
                        
                        self.viewFront.isHidden = false
                        self.hghtViewFrnt.constant = 130
                        
                        isFrntSelect = "true"
                        
                        self.imgfrontdl.setIndicatorStyle(UIActivityIndicatorView.Style.medium)
                        self.imgfrontdl.setShowActivityIndicator(true)
                        self.imgfrontdl.sd_setImage(with: URL.init(string: "\(Constant.img_base_Url)\(dl_front_snap)"), placeholderImage: UIImage(named: ""))
                        
                    }
                    
                    if let dl_back_snap = dataD["dl_back_snap"] as? String{
                        
                        isBackSelect = "true"
                        self.viewBack.isHidden = false
                        self.hghtViewBack.constant = 130
                        
                        self.imgbackdl.setIndicatorStyle(UIActivityIndicatorView.Style.medium)
                        self.imgbackdl.setShowActivityIndicator(true)
                        self.imgbackdl.sd_setImage(with: URL.init(string: "\(Constant.img_base_Url)\(dl_back_snap)"), placeholderImage: UIImage(named: ""))
                    }
                }
            }
            
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
        
    }
}
