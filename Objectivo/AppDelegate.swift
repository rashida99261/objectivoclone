//
//  AppDelegate.swift
//  Objectivo
//
//  Created by Apple on 17/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Reachability

let reach = try? Reachability()
var userDef = UserDefaults.standard

var isResetPassFrom = ""


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        let _window = UIWindow(frame: UIScreen.main.bounds)
        window = _window
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let initialViewController = storyboard.instantiateViewController(withIdentifier: "splashViewController") as! splashViewController
        let rootViewController = initialViewController
        let navigationController = UINavigationController()
        navigationController.navigationBar.isHidden = true
        navigationController.viewControllers = [rootViewController]
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
        return true
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        print("foreground")
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        print("active")
    }
   
    func applicationWillResignActive(_ application: UIApplication) {
        print("resign active")
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        print("bg")
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        print("terminate")
    }
}

